**Note**: This is a prototype for a RTS game (Inspired by the FPS game Brothers in Arms) built in Unity around 2014/2017. The project was mainly a learning experience and got discontinued in 2017. The focus of this project was on enemy AI, pathfinding, character controllers, VFXs and UI. Might pick it up again some day, lower the scope, who knows? I would probably rewrite a big chunk. My work was mostly coding, the assets are from the Unity Asset Store. I did create character rigs and used animations from other sources.

If you are interested in testing the prototype, you can **download** a Windows Build from the repository files. **Windows Build.rar**

**Scroll down for a Gameplay Video.**

## Game Overview
“The game” is a Real Time Strategy game where the player is in command of a number of units. The objectives range from defending to attacking strategic points. In order to complete these goals the player has to order its units teams to eliminate the enemies that he finds in his path.The enemies are well organized and will force the player to play tactically, by making use of flanking maneuvers. 
The game takes place in maps based on battles of the Second World War. These maps will provide different elements which allow for flanking tactics for both the player and its enemies.

## Gameplay and Mechanics
The basic gameplay centers around the infantry squads made up of teams. This includes the **Assault Team** and the **Base of Fire Team**. Each team is represented by a unique icon.  Each of the teams has different functions based on their weapons carried. The player can use either team as they wish, but using the appropriate team for the appropriate purpose will increase the effectiveness of the squad as a whole. 

- MG Team - The MG team usually carries **machineguns**. These are usually heavier weapons that can provide a steady, yet heavy base of fire. The fire team's job is to use overwhelming firepower on the enemy so that they stay behind over and are "suppressed." Suppressed enemies will fire less and with less accuracy, allowing the player and his other squads to maneuver more easily. 

![](docs/images/mgTeamUI.png)

- Base of Fire Team - The fire team usually carries **semi automatic rifles**. The fire team's job is to use overwhelming firepower on the enemy so that they stay behind over and are "suppressed." Suppressed enemies will fire less and with less accuracy, allowing the player and his other squads to maneuver more easily. 

![](docs/images/bofTeamUI.png)

- Assault Team - Usually carries **rifles** and **submachine guns**. These are lighter and have relatively high rates of fire. This allows the assault team to move more quickly and dart between cover. The Assault team is used to flank the enemy once the fire team has suppressed them. When they are close enough to the target, they can make use of **grenades** to quickly destroy it.

![](docs/images/assaultTeamUI.png)

Teams can be ordered to move, lay suppressive fire, find cover, and charge the enemy. The game stresses at multiple points the effectiveness of fire and maneuver tactics, known as the Four F’s. The "Four F's"' is a military term used during WWII used to describe the basics of infantry combat. 

- Find - Locate the enemy and determine their exact position 

- Fix - Use gunfire to pin, or suppress, the enemy. 

- Flank - Send other soldiers around to the sides of the enemy position. 

- Finish - Eliminate all enemy combatants. 

## Combat Prototype
As the player enters the area, they are either fired upon by the enemy (giving away their location) or the player sees the enemy first. 
The player orders his fire teams to intensify their fire on a specific target. Until they are given a new order, they will concentrate their fire as intensively as they can to suppress (pin down) the enemy target.
The **suppression indicator** over the enemy is used to show the target suppression level. **Red means unsuppressed, grey is suppressed**. When an enemy is suppressed they will spend more time in cover and less time firing. Their shots will also become less frequent and less accurate.
By sending their assault squads the player gets to the sides (flank) of the enemy and finishes them off. 

![](docs/images/combatgif.gif)

## Prototype UI
The following image shows an overview of the game UI:

![](docs/images/uiDetails.png)

When a unit is selected, the UI for the unit Fire Team will be displayed. By default it is closed, you can click on the Fire Team Name to open/close the unit details.

## Pause Menu
The Pause Menu has been disabled as it was done using the Legacy UI system.

## Basic Controls
- **WASD** or **Arrow Keys** to move the camera around
- **Scroll up/down** will move and rotate the camera up and down
- You can select individual units using **click** or **selection box**
- **Right click** to move units to a location
- **Left click** to unselect units
- Press the **1** key to select the **Assault Team**
- Press the **2** key to select the **Base of Fire Team**
- Press the **3** key to select the **MG Team**
- **Double tap** the team selection keys to center the camera on them

## Gameplay Video

[![Watch the video](https://img.youtube.com/vi/NN_1htybAYY/hqdefault.jpg)](https://youtu.be/NN_1htybAYY)