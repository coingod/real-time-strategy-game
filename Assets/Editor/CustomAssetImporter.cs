﻿// -------------------------------------------------------------------
// - A custom asset importer for Unity 3D game engine by Sarper Soher-
// - http://www.sarpersoher.com                                      -
// - http://www.sarpersoher.com/a-custom-asset-importer-for-unity/   -
// -------------------------------------------------------------------
// - This script utilizes the file names of the imported assets      -
// - to change the import settings automatically as described        -
// - in this script                                                  -
// -------------------------------------------------------------------


using UnityEngine;
using UnityEditor; 

// We inherit from the AssetPostProcessor class which contains all the exposed variables and event triggers for asset importing pipeline
internal sealed class CustomAssetImporter : AssetPostprocessor
{
    #region Methods

    //-------------Pre Processors

    // This event is raised when a texture asset is imported
    private void OnPreprocessTexture()
    {

    }

    // This event is raised when a new mesh asset is imported
    private void OnPreprocessModel()
    {
        // Once again I unbox the assetImporter reference, to a ModelImporter this time
        var importer = assetImporter as ModelImporter;

        //importer.globalScale = 1;
        importer.optimizeMesh = true;
        importer.importBlendShapes = false;
        importer.keepQuads = false;
        importer.weldVertices = true;
        importer.importVisibility = false;
        importer.importCameras = false;
        importer.importLights = false;
        importer.swapUVChannels = false;

        //In order to get a LowPoly style, we need not to have smooth meshes
        importer.importNormals = ModelImporterNormals.Calculate;
        importer.normalCalculationMode = ModelImporterNormalCalculationMode.Unweighted_Legacy;
        importer.normalSmoothingAngle = 0;
        importer.importTangents = ModelImporterTangents.CalculateLegacy;

        //Materials are handled from Unity
        importer.importMaterials = false;

        //Animation is imported separatly
        importer.importAnimation = false;
    }

    // This event is raised every time an audio asset is imported
    // This method does nothing at the moment, just a skeleton to fill in if we ever need to do audio specific importing
    // Imports audio assets in the default way without changing anything
    private void OnPreprocessAudio()
    {
        var fileNameIndex = assetPath.LastIndexOf('/');
        var fileName = assetPath.Substring(fileNameIndex + 1);

        if (!fileName.Contains("snd")) return;

        var importer = assetImporter as AudioImporter;
        if (importer == null) return;
    }

    //-------------Post Processors

    // This event is called as soon as the texture asset is imported successfully
    // Does nothing currently, just here for future possibilities
    private void OnPostprocessTexture(Texture2D import) { }

    // This event is called as soon as the mesh asset is imported successfully
    private void OnPostprocessModel(GameObject import)
    {
        // Sometimes the artist who created the model forgets to "freeze" the position and rotation of the mesh
        // I find it dirty and telling an artists to fix and re-export the same mesh pisses them off especially if you are working on a game with a lot of assets
        // So OnPostprocessModel() to the rescue!
        // We simply zero out the position and rotation fields so when we put these models in our scene they don't come with their saved transform data
        //import.transform.position = Vector3.zero;
        //import.transform.rotation = Quaternion.identity;
    }

    // This event is called as soon as the audio asset is imported successfully
    private void OnPostprocessAudio(AudioClip import) { }

    #endregion
}