﻿using UnityEngine;

[ExecuteInEditMode]
public class Debuger : MonoBehaviour
{
	private Transform cam;
	private Vector3 input;

	private void Awake()
	{
		cam = Camera.main.transform;
	}

	private void Update ()
	{
	
		input = Input.acceleration;
	}

	private void OnGUI ()
	{
		GUI.Label (new Rect (25, 25, 500, 30), "Camera Position:");
		GUI.Label (new Rect (30, 45, 500, 30), "("+cam.position.x+", "+cam.position.y+", "+cam.position.z+")");
		GUI.Label (new Rect (25, 65, 500, 30), "Camera Rotation:");
		GUI.Label (new Rect (30, 85, 500, 30), "("+cam.rotation.x+", "+cam.rotation.y+", "+cam.rotation.z+")");
		GUI.Label (new Rect (25, 105, 500, 30), "Accelerometer input:");
		GUI.Label (new Rect (30, 125, 500, 30), "("+input.x+", "+input.y+", "+input.z+")");
	}
}
