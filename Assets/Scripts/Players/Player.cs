﻿using UnityEngine;
using System.Collections.Generic;

//Player Class
//Represents a Human or AI Player, with information about the Units it can control.
public class Player : MonoBehaviour
{
    /* Player properties */
    public string UserName;                                 //The Player UserName
    public bool Human;                                      //Whether this player is human controlled or AI controlled 
    public Color TeamColor;                                 //The Player Team color

    public Transform fireTeams;                            //The Player units
    public List<Unit> selectionList;                        //Currently selected Units
    public List<FireTeam> selectedTeams;                    //Currently selected Fire Teams

    private HUD hud;

    /* Optimization Variables */
    private Transform tr;                                   //Cached Transform Component

    private void Awake()
    {
		tr = transform;
        hud = GetComponent<HUD>();
    }
	
	private void Start ()
    {
		selectionList = new List<Unit>();
        selectedTeams = new List<FireTeam>();
        //Find the Units assigned to this Player
        fireTeams = tr.Find ("Units");
        //Register this Player in the Unit Manager
		UnitManager.AddPlayer(this);
	}
	
    public List<FireTeam> GetAllFireTeams()
    {
        List<FireTeam> teams = new List<FireTeam>();
        foreach (Transform ft in fireTeams)
            teams.Add(ft.GetComponent<FireTeam>());
        return teams;
    }

    public List<Unit> GetSelectedUnits()
    {
        return selectionList;
    }

    public FireTeam GetFireTeam(int fireTeamId)
    {
		return fireTeams.GetChild(fireTeamId).GetComponent<FireTeam>();
	}

	public int GetFireTeamsCount()
    {
		return fireTeams.childCount;
	}
	
    public bool OwnerOfUnit(Unit unit)
    {
        bool owner = false;
        foreach (Transform ft in fireTeams)
            owner = owner || ft.GetComponent<FireTeam>().units.Contains(unit);
        return owner;
    }

	public int GetUnitsCount()
    {
		int c = 0;
		foreach(Transform t in fireTeams)
        {
			FireTeam s = t.GetComponent<FireTeam>();
			if(s)
            {
				foreach(Unit u in s.units)
					c++;
			}
		}
		return c;
	}
	
	public void AddSelection(Unit u)
	{
		if(!selectionList.Contains(u))
		{
			selectionList.Add(u);
			u.SetSelectedState(true);
			if(!selectedTeams.Contains(u.GetFireTeam()))
            {
                selectedTeams.Add(u.GetFireTeam());
                hud.OnSelectionChange();
            }
        }
	}

	public void RemoveSelection(Unit unit)
	{
		if(selectionList.Contains(unit))
		{
			selectionList.Remove(unit);
			unit.SetSelectedState(false);
			if(selectedTeams.Contains(unit.GetFireTeam()))
            {
                selectedTeams.Remove(unit.GetFireTeam());
                hud.OnSelectionChange();
            }
        }
	}
	
	public void ResetSelection()
	{
		foreach (Unit u in selectionList)
        {
			u.SetSelectedState (false);
		}
		selectedTeams.Clear();
		selectionList.Clear();
        hud.OnSelectionChange();
    }

	//Creates a 2D box area with 2 Vectors.
	//Checks which units are inside that area.
	public void BoxSelection(Vector3 from, Vector3 to)
	{
		ResetSelection();
		
		foreach (Transform ft in fireTeams)
			foreach (Transform u in ft)
			    if (((u.position.x > from.x && u.position.x < to.x) || (u.position.x < from.x && u.position.x > to.x)) &&
				    ((u.position.z > from.z && u.position.z < to.z) || (u.position.z < from.z && u.position.z > to.z)))
			    {
				    AddSelection(u.GetComponent<Unit>());
			    }
	}
}