﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class FireTeam : MonoBehaviour
{
    [Header("Fire Team Properties")]
	public Texture2D icon;                                  //This Fire Team icon
    public List<Unit> units;                                //Units in this team
    public Unit leader;                                     //The Leader Unit
    public int maxUnits;

    [Header("Fire Team Health")]
    private float maxHealth = 100;
    public float currentHealth;

    [Header("Fire Team Suppresion")]
    private float maxSuppresion = 100;
    public float currentSuppresion;

    /* Private variables */
    private Player player;
    private Vector3[] unitPositions;

    private void Awake()
    {
		units = new List<Unit>();
	}
	
	// Use this for initialization
	private void Start ()
    {
		//squadPerformance = GetComponent<SquadPerformance>();
		foreach(Transform t in transform)
        {
			Unit u = t.GetComponent<Unit>();
			u.SetFireTeam(this);
			units.Add(u);
		}
		maxUnits = units.Count;

        currentHealth = maxHealth;

        unitPositions = new Vector3[units.Count];

		if(!leader)
			leader = GetComponentInChildren<Unit>();
	}
	
	// Update is called once per frame
	private void Update ()
    {
        if (GetUnitsCount() == 0)
            return;

        UpdateFireTeamSuppresion();

        //Updates the FireTeam tranform position to the average position of the units
        //Then relocates the units local position back to their original positions
        int i = 0;

		Vector3 avg = Vector3.zero;
		foreach(Unit u in units) {
			avg.x += u.transform.position.x;
			avg.y += u.transform.position.y;
			avg.z += u.transform.position.z;

			unitPositions[i++] = u.transform.position;
		}
		avg /= units.Count;

		transform.position = avg;

		i = 0;

		foreach(Unit u in units) {
			u.transform.position = unitPositions[i++];
		}		
	}

	public Vector3 FireTeamPosition()
    {
		Vector3 avg = Vector3.zero;
		foreach(Unit u in units) {
			avg.x += u.transform.position.x;
			avg.y += u.transform.position.y;
			avg.z += u.transform.position.z;
		}
		return avg / units.Count;
	}

	public Unit GetUnit(int i)
    {
		return units[i];
	}
	
	public void AddUnit(Unit u)
    {
		units.Add(u);
	}
	
	public void RemoveUnit(int i)
    {
		units.RemoveAt(i);
	}
	
	public void RemoveUnit(Unit i)
    {
		units.Remove(i);

        if (GetUnitsCount() == 0)
        {
            Destroy(gameObject, 1);
        }

        if (!leader)
            leader = GetComponentInChildren<Unit>();
    }
	
	public int GetUnitsCount()
    {
		return units.Count;
	}
	
    public void HoldFire()
    {
        foreach (Unit x in units)
        {
            x.holdFire = !x.holdFire;
        }
    }

    public void UpdateFireTeamHealth()
    {
        currentHealth = 0;
        foreach (Unit u in units)
            currentHealth += u.health;

        currentHealth /= maxUnits;

        /*
        //If there is a health slider, update its value
        if (healthSlider != null)
            healthSlider.value = currentHealth;
        */
    }

    public void UpdateFireTeamSuppresion()
    {
        currentSuppresion = 0;
        foreach (Unit u in units)
            currentSuppresion += u.suppresion.GetComponent<Suppresion>().level;

        currentSuppresion /= maxUnits;

        /*
        //If there is a health slider, update its value
        if (suppresionSlider != null)
            suppresionSlider.value = currentSuppresion;
        */
    }
}
