﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitSpeech : MonoBehaviour
{
    private AudioSource audioSource;
    public bool speaking = false;
    public AudioClip[] currentAudioSet;
    private Unit unit;
    public bool german = false;
    private bool newTarget = true;

    // Use this for initialization
    private void Start ()
    {
        audioSource = GetComponent<AudioSource>();
        unit = GetComponent<Unit>();
    }
	
	// Update is called once per frame
	private void Update ()
    {
        if(!speaking)
        {
            if(unit.target && newTarget)
            {
                newTarget = false;
                if (german)
                    currentAudioSet = GameManager.Instance.ger_attack;
                else
                    currentAudioSet = GameManager.Instance.rus_attack;
            }
            else if (unit.attacking)
            {
                if (german)
                    currentAudioSet = GameManager.Instance.ger_onCombat;
                else
                    currentAudioSet = GameManager.Instance.rus_onCombat;
            }
            else if (unit.moving)
            {
                if (german)
                    currentAudioSet = GameManager.Instance.ger_move;
                else
                    currentAudioSet = GameManager.Instance.rus_move;
            }

            if (currentAudioSet.Length > 0)
            {
                StartCoroutine("Speak");
            }
        }

        if (!unit.target)
            newTarget = true;

    }

    private IEnumerator Speak()
    {
        speaking = true;

        audioSource.clip = currentAudioSet[Random.Range(0, currentAudioSet.Length - 1)];
        audioSource.Play();
        yield return new WaitForSeconds(Random.Range(6f, 12f));

        currentAudioSet = new AudioClip[0];
        speaking = false;
    }
}
