﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof (UnitSight))]
public class Unit : CharacterMotor
{
    [Header("Unit State")]
    public bool holdFire = true;                            //Is this Unit holding his fire?
    public bool attacking = false;                          //Is this Unit attacking?
    public bool aiming = false;                             //Is this Unit aiming at it's Target?
    public bool reloading = false;                          //Is this Unit reloading his weapon?

    [Header("Unit Health")]
    public float health = 100;                              //Current health this unit has

    [Header("Cover Mechanics")]
    public bool takingCover = false;                        //Is this Unit taking cover?
    public bool exposed = false;                            //Is this Unit exposing himself to enemy fire?
    public bool findCover = true;                           //Will this Unit move to cover?
	public float coverSearchRadius = 10f;                   //Radius this Unit will search for cover
    public float exposureTime = 2f;                         //How much time the Unit will expose himself to fire at the enemy
    public float timeSinceLastExposure = 0f;                //The amount of time elapsed since last exposed himself
    private CoverNode currentCoverNode;                     //Current cover node this unit is taking cover in
    public bool debugCover = false;                         //Draw information related to how the Unit chooses cover positions

    [Header("Unit Weapon")]
    public Transform[] weapons;                             //Unit Weapons
    public Weapon currentWeapon;                            //Current equipped Weapon
    public Transform weaponsTransform;                      //Weapons root Transform
    private bool canUseGrenades = false;                    //Has this Unit grenades?

    /*  References to other scripts */
    private Player player;                                  //The Player that owns this unit
    private UnitSight sight;                                //The sight script of this unit
    private FireTeam fireTeam;                              //The Fire Team this unit belongs to
    private Animator anim;                                  //The Animator Component attached to this unit
    public Suppresion suppresion;                           //The suppresion metter for this unit
    private AudioSource audioSource;                        //The Audio Source of this unit

    /*  Private variables */
    private GameObject selectionRing;                       //The Ring instance that displays if this unit is currently selected 
    private Quaternion aimRotation;                         //Current rotation this unit is aiming at
    private CapsuleCollider capsule;                        //The Collider this Unit has
    private Unit lastDamagedBy;                             //Reference to the last enemy Unit to damage this unit
    private float capsuleHeight;                            //This unit's standing height 
    private Vector3 capsuleCenter;                          //This unit's collision capsule center
    private bool crounching = false;                        //Is this unit currently crounched?
    private float reloadStandingMultiplier;                 //Speed multiplier for Standing Reloading Animation
    private float reloadSquatMultiplier;                    //Speed multiplier for Crounched Reloading Animation
    private float doubleClickCooldown = 0.5f;               //Cooldown timer to check double clicks

    protected override void Awake()
    {
        base.Awake();
        sight = GetComponent<UnitSight>();
        anim = GetComponent<Animator>();
        suppresion = GetComponentInChildren<Suppresion>();
        capsule = GetComponent<CapsuleCollider>();
        audioSource = GetComponent<AudioSource>();
        player = transform.root.GetComponent<Player>();
        //weaponsTransform = tr.Find("Weapon");
        if (Player) SetTeamColor();
    }
    // Use this for initialization
    protected override void Start ()
    {
        base.Start();
        capsuleHeight = capsule.height;
        capsuleCenter = capsule.center;
        foreach (Transform wp in weapons)
        {
            //Transform gunTr = (Transform) Instantiate(wp,weaponsTransform.position,tr.rotation);
            Transform gunTr = (Transform)Instantiate(wp, weaponsTransform);
            gunTr.parent = weaponsTransform;

            ///Try to instantiate the weapon in the correct place instead of doing this !
            gunTr.localRotation = Quaternion.identity;
            gunTr.localPosition = Vector3.zero;
            ///

            Weapon instance_wp = gunTr.GetComponent<Weapon>();
            instance_wp.SetOwner(this);
			if(!currentWeapon)
                currentWeapon = gunTr.GetComponent<Weapon>();

            if (instance_wp.throwable)
                canUseGrenades = true;
		}

        if (!rigid)
        {
            CheckGroundStatus();
            tr.position = new Vector3(tr.position.x, m_GroundPoint.y, tr.position.z);
        }

        UpdateAnimClipTimes();
    }

    public void UpdateAnimClipTimes()
    {
        if (!currentWeapon)
            return;

        AnimationClip[] clips = anim.runtimeAnimatorController.animationClips;
        foreach (AnimationClip clip in clips)
        {
            switch (clip.name)
            {
                case "Reload_standing":
                    reloadStandingMultiplier = clip.length / currentWeapon.reloadTime;
                    break;
                case "Reload_squat":
                    reloadSquatMultiplier = clip.length / currentWeapon.reloadTime;
                    break;
            }
        }
    }

    public void SetSelectedState( bool value )
    {
		if(value)
        {
            selectionRing = Instantiate(GameManager.Instance.selectionRing, tr.position, Quaternion.LookRotation(Vector3.up)) as GameObject;
            selectionRing.transform.parent = tr;
		}
		else 
			Destroy(selectionRing);

	}

	public bool IsMoving()
    {
		return moving || movingIntoPosition;
	}

	public bool TakingCover()
    {
		return takingCover;
	}

	public void SetFireTeam(FireTeam ft)
    {
		fireTeam = ft;
	}

	public FireTeam GetFireTeam()
    {
		return fireTeam;
	}

	public Weapon GetCurrentWeapon()
    {
		return currentWeapon;
	}

	public Player Owner()
    {
		return Player;
	}

    public Unit LastDamagedBy
    {
        get
        {
            return lastDamagedBy;
        }

        set
        {
            lastDamagedBy = value;
        }
    }

    public Player Player
    {
        get
        {
            return player;
        }

        set
        {
            player = value;
        }
    }

    public void SwitchWeapon(Weapon wp)
    {
        currentWeapon = wp;
    }

    // Called when an Enemy Chacracter is killed by a bullet from this Unit
    public void OnEnemyKilled()
    {
        AudioClip[] enemyDown;
        if(Player.Human)
            enemyDown = GameManager.Instance.ger_enemyDown;
        else
            enemyDown = GameManager.Instance.rus_enemyDown;

        audioSource.clip = enemyDown[Random.Range(0, enemyDown.Length - 1)];
        audioSource.Play();
    }

    // Called when this Unit-s weapon is reloading
    public void OnWeaponReload()
    {
        //33% chances of saying something
        if (Random.Range(0, 4) != 0)
            return;

        AudioClip[] reloading;
        if (Player.Human)
            reloading = GameManager.Instance.ger_reloading;
        else
            reloading = GameManager.Instance.rus_reloading;

        audioSource.clip = reloading[Random.Range(0, reloading.Length - 1)];
        audioSource.Play();
    }

    // Update is called once per frame
    public override void Update ()
    {
		base.Update();

        if(currentWeapon)
            reloading = currentWeapon.Reloading();

        if (!exposed && suppresion.suppresed)
            timeSinceLastExposure += Time.deltaTime - Time.deltaTime * 0.75f * (1 - suppresion.GetPercentage());
        else if (!exposed)
            timeSinceLastExposure += Time.deltaTime - Time.deltaTime * 0.5f * (1 - suppresion.GetPercentage());
        else
            timeSinceLastExposure += Time.deltaTime;

        //If I have a Target in range Attack it
        if (!holdFire && !reloading && target && TargetInRange() && ReadyToExpose())
        {
            //If has grenades and can be used switch
            if (canUseGrenades && TargetInCloseRange())
                SwitchWeapon(weaponsTransform.GetChild(1).GetComponent<Weapon>());
            else
                SwitchWeapon(weaponsTransform.GetChild(0).GetComponent<Weapon>());

            PerformAttack();
        }
        else
            attacking = false;

        //Handle Movement
        if (canMove)
        {
			if(moving)
            {
				Vector3 dir = CalculateVelocity (tr.position);
				
				if (targetDirection != Vector3.zero)
					RotateTowards (targetDirection);

                tr.Translate(dir * Time.deltaTime, Space.World);

                CheckGroundStatus();
                tr.position = new Vector3(tr.position.x, m_GroundPoint.y, tr.position.z);
            }
            
        }

		if(aiming)
        {
			transform.rotation = Quaternion.RotateTowards(transform.rotation, aimRotation, currentWeapon.aimSpeed);
			//sometimes it gets stuck exactly 180 degrees out in the calculation and does nothing, this check fixes that
			Quaternion inverseAimRotation = new Quaternion(-aimRotation.x, -aimRotation.y, -aimRotation.z, -aimRotation.w);
			if(transform.rotation == aimRotation || transform.rotation == inverseAimRotation)
				aiming = false;
		}

        ScaleCapsuleForCrouching();

        //Animation
        if (anim)
        {
            anim.SetBool("moving", moving);
            anim.SetBool("reloading", reloading);
            anim.SetBool("cover", takingCover);
            anim.SetBool("attacking", attacking);
            anim.SetFloat("reloadStandingMultiplier", reloadStandingMultiplier);
            anim.SetFloat("reloadSquatMultiplier", reloadSquatMultiplier);
        }

        doubleClickCooldown -= Time.deltaTime;
        if (doubleClickCooldown < 0)
            doubleClickCooldown = 0;
    }

    void ScaleCapsuleForCrouching()
    {
        //if (takingCover && !moving)
        if(anim.GetCurrentAnimatorStateInfo(0).IsName("Combat_Mode_Squat") || anim.GetCurrentAnimatorStateInfo(0).IsName("Reload_squat"))
        {
            if (crounching)
                return;
            //Change collider height to make it shorter
            capsule.height = capsule.height / 1.75f;
            capsule.center = capsule.center / 1.75f;
            crounching = true;
        }
        else
        {
            //Change collider height to make it taller
            Ray crouchRay = new Ray(tr.position + Vector3.up * capsule.radius * 0.5f, Vector3.up);
            float crouchRayLength = capsuleHeight - capsule.radius * 0.5f;
            if (Physics.SphereCast(crouchRay, capsule.radius * 0.5f, crouchRayLength, Physics.AllLayers, QueryTriggerInteraction.Ignore))
            {
                crounching = true;
                return;
            }
            capsule.height = capsuleHeight;
            capsule.center = capsuleCenter;
            crounching = false;
        }
    }

    public bool ReadyToExpose()
    {
        //If we are not taking cover, we are already exposed
        if (!takingCover || currentWeapon.name == "MG34(Clone)")
            return true;

        if (timeSinceLastExposure > exposureTime)
        {
            timeSinceLastExposure = 0f;
            exposed = !exposed;
            return true;
        }

        return false || exposed;
    }
    
    public void MouseClick(GameObject hitObject, Vector3 hitPoint)
    {
        //if(hitObject.name == "Ground")
        if (hitObject.tag == "Ground")
        {
			float x = hitPoint.x;
			//makes sure that the unit stays on top of the surface it is on
			float y = hitPoint.y;// + player.SelectedObject.transform.position.y;
			float z = hitPoint.z;
			if (!findCover)
				SearchPath(new Vector3(x, y, z));
			else 
				FindCover(new Vector3(x, y, z));
		}
        else if (hitObject.tag != "Building")
        {
            target = hitObject.transform;
        }

        if (doubleClickCooldown > 0)
            speed = runSpeed;
        else
            doubleClickCooldown = 0.5f;
    }

	public void FindCover( Vector3 pos )
    {
        int layerMask = 1 << 2;//Layer 2, Ignore Raycast
        Collider[] hitCovers = Physics.OverlapSphere(pos, coverSearchRadius, layerMask);
        if (hitCovers.Length > 0)
        {
            if (currentCoverNode) currentCoverNode.inUse = false;
            CoverNode cn = FindClosestCoverNode(pos, hitCovers);
            if (cn)
            {
                cn.inUse = true;
                //if (currentCoverNode) currentCoverNode.inUse = false;
                currentCoverNode = cn;
                takingCover = true;
                SearchPath(cn.transform.position);//Move to that cover node position
            }
            else
            {
                SearchPath(pos);//If we didn't find a CoverNode just go to ordered position
                takingCover = false;
            }
        }
        else
        {
            SearchPath(pos);//If we didn't find Cover just go to ordered position
            takingCover = false;
        }
    }

	public override void SearchPath(Vector3 destination)
    {
		base.SearchPath (destination);

        //Adjust Destination Height for the Ring Effect below
        RaycastHit hitInfo;
        if (Physics.Raycast(destination + Vector3.up, Vector3.down, out hitInfo, 1f))
            destination = hitInfo.point;

        //Instantiate target position ring effect
        Instantiate(GameManager.Instance.orderRing, destination, Quaternion.LookRotation(Vector3.up));
	}

    public void Damage(Unit attacker, float amount)
    {
        if(attacker.Player != this.Player)
            lastDamagedBy = attacker;

        Damage(amount);
    }

    //Damages the unit, returns true if unit dies
    public void Damage(float amount)
    {
        //Avoid killing this unit more than once at the same time
        if (health == 0)
            return;

        health = (amount > health) ? 0 : (health - amount);
        fireTeam.UpdateFireTeamHealth();

        if (health == 0)
            Die();
    }

    protected void Die()
    {
        //Alert the attacker that he killed this Unit
        if(LastDamagedBy)
            LastDamagedBy.OnEnemyKilled();

        //Clean references
        Player.RemoveSelection(this);
        fireTeam.RemoveUnit(this);
        if(currentCoverNode)
            currentCoverNode.inUse = false;
        //Suppres nearby Units
        Collider[] colliders = Physics.OverlapSphere(transform.position, 5f);
        for (int i = 0; i < colliders.Length; i++)
        {
            Suppresion nearby = colliders[i].GetComponent<Suppresion>();
            if (nearby)
                nearby.Suppress(100);
        }
        //Instantiate corpse and effects
        GameObject corpse = Instantiate(GameManager.Instance.ragdoll, tr.position, tr.rotation);
        Instantiate(GameManager.Instance.bloodSplats, tr.position + Vector3.up * 0.1f, Quaternion.Euler(Vector3.left * 90));

        if (Player.Human)
            corpse.GetComponent<PlayRandomDeadSound>().PlayGermanVO();
        else
            corpse.GetComponent<PlayRandomDeadSound>().PlayRussianVO();

        //Destroy Unit
        Destroy(gameObject);
    }

	//Check if the target is close enough to engage
	protected bool TargetInRange()
    {
		Vector3 targetLocation = target.position;
		Vector3 direction = targetLocation - transform.position;
		if(direction.sqrMagnitude < sight.viewRange * sight.viewRange)
			return true;
		return false;
	}

    //Check if the target is close enough to throw grenades
    protected bool TargetInCloseRange()
    {
        Vector3 targetLocation = target.position;
        Vector3 direction = targetLocation - transform.position;
        if (direction.sqrMagnitude < (sight.viewRange * sight.viewRange)/4)
            return true;
        return false;
    }

    //Attacks the target
    protected void PerformAttack()
    {
        //Debug.Log("Attacking!");
        if (!target)// || suppresion.suppresed)//suppresion.level < 50)
            return;

        attacking = true;

        if (!TargetInRange())
            AdjustPosition();
		else if(!TargetInFrontOfWeapon())
            AimAtTarget();
		else if(ReadyToFire())
			UseWeapon();
	}

	//Relocates the unit to a position in which he can engage the target
	protected void AdjustPosition()
    {
        //Debug.Log("Adjusting Position!");
        movingIntoPosition = true;
		Vector3 attackPosition = FindNearestAttackPosition();
		if (!findCover)
			SearchPath(attackPosition);
		else 
			FindCover(attackPosition);
	}

	//Calculates closest position to engage the target
	protected Vector3 FindNearestAttackPosition()
    {
		Vector3 targetLocation = target.position;
		Vector3 direction = targetLocation - transform.position;
		float targetDistance = direction.magnitude;
		float distanceToTravel = targetDistance - (0.9f * sight.viewRange);
		return Vector3.Lerp(transform.position, targetLocation, distanceToTravel / targetDistance);
	}

	//Checks if the unit is aiming at the target
	protected bool TargetInFrontOfWeapon()
    {
		Vector3 targetLocation = target.position;
		Vector3 direction = targetLocation - transform.position;
        //return (direction.normalized == transform.forward.normalized);
        return Vector3.Dot(direction.normalized, transform.forward.normalized) >= 0.999f;   //Dot product allows the unit to shoot when his targget is moving
	}

	//Calculates aim direction towards the target
	protected void AimAtTarget ()
    {
        //Debug.Log("Aiming!");
        aiming = true;
		aimRotation = Quaternion.LookRotation (target.position - transform.position);
	}

	protected bool ReadyToFire()
    {
        return currentWeapon.ReadyToFire() && anim.GetCurrentAnimatorStateInfo(0).IsName("Combat_Mode_Standing");
	}

	protected virtual void UseWeapon()
    {
        currentWeapon.Fire ();
	}

	protected void SetTeamColor()
    {
		TeamColor[] teamColors = GetComponentsInChildren<TeamColor>();
		foreach(TeamColor teamColor in teamColors) teamColor.GetComponent<Renderer>().material.color = Player.TeamColor;
	}

    public void SetTeamColor(float mood)
    {
        TeamColor[] teamColors = GetComponentsInChildren<TeamColor>();
        foreach (TeamColor teamColor in teamColors)
        {
            //Calculate the color
            float moodRatio = mood / 100;
            teamColor.GetComponent<Renderer>().material.color = new Color(
                Color.red.r * (1 - moodRatio) + Player.TeamColor.r * moodRatio,
                Color.red.g * (1 - moodRatio) + Player.TeamColor.g * moodRatio,
                Color.red.b * (1 - moodRatio) + Player.TeamColor.b * moodRatio
                );
        }
    }

    public CoverNode FindClosestCoverNode(Vector3 pos, Collider[] coverNodes)
    {
        List<CoverNode> goodCoverNodes = new List<CoverNode>();
        List<CoverNode> goodCoverNodesFromEnemy = new List<CoverNode>();
        foreach (Collider col in coverNodes)
        {
            CoverNode cn = col.GetComponent<CoverNode>();
            if (!cn)
                continue;

            if (!cn.inUse)
            {
                if (GoodCover(cn, pos))
                {
                    if (GameManager.Instance.coverFromTarget && GoodCoverFromEnemy(cn))
                        goodCoverNodesFromEnemy.Add(cn);
                    else
                        goodCoverNodes.Add(cn);
                    /*
                    if (!GameManager.Instance.coverFromTarget)
                        goodCoverNodes.Add(cn);
                    else if (GoodCoverFromEnemy(cn))
                        goodCoverNodes.Add(cn);
                    */
                }
            }
        }
        //if(debugCover) Debug.Log("Good CoverNodes: "+goodCoverNodes.Count);
        if (goodCoverNodesFromEnemy.Count > 0)
        {
            CoverNode closest = goodCoverNodesFromEnemy[0];
            Vector3 closestPos = closest.transform.position;
            foreach (CoverNode c in goodCoverNodesFromEnemy)
            {
                if (Vector3.Distance(pos, c.transform.position) < Vector3.Distance(pos, closestPos))
                {
                    closest = c;
                    closestPos = closest.transform.position;
                }
            }
            return closest;
        }
        else if(goodCoverNodes.Count > 0)
        {
            CoverNode closest = goodCoverNodes[0];
            Vector3 closestPos = closest.transform.position;
            foreach (CoverNode c in goodCoverNodes)
            {
                if (Vector3.Distance(pos, c.transform.position) < Vector3.Distance(pos, closestPos))
                {
                    closest = c;
                    closestPos = closest.transform.position;
                }
            }
            return closest;
        }
        return null;
    }

    public bool GoodCoverFromEnemy(CoverNode cn)
    {
        Vector3 cNodePos = cn.transform.position;
        RaycastHit hit;
        bool good = true;
        Player enemy = UnitManager.GetPlayer(0);
        int j = 0; int i = 0;
        while (j < enemy.GetFireTeamsCount() && good)
        {
            while (i < enemy.GetFireTeam(j).GetUnitsCount() && good)
            {
                Vector3 targetPos = UnitManager.GetUnit(0, j, i).transform.position;
                if (debugCover) Debug.DrawRay(targetPos + Vector3.up, cNodePos - targetPos, Color.red, 5.0f);

                //If the Ray doesn't hit something on the way, we can be seen!
                if (!Physics.Raycast(targetPos + Vector3.up, cNodePos - targetPos, out hit, Vector3.Distance(targetPos, cNodePos)))
                    good = false;
                //else if (debugCover) Debug.Log("Target: " + i + " Hit: " + hit.collider.gameObject.name);

                i++;
            }
            j++;
        }
        return good;
    }

    public bool GoodCover(CoverNode cn, Vector3 movePos)
    {
        Vector3 cNodePos = cn.transform.position;
        RaycastHit hit;

        if (debugCover) Debug.DrawRay(movePos + Vector3.up, cNodePos - movePos, Color.blue, 5.0f);

        if (Physics.Raycast(movePos + Vector3.up, cNodePos - movePos, out hit, Vector3.Distance(movePos, cNodePos)))
        {
            //if(debugCover) Debug.Log ("Hit: "+hit.collider.gameObject.name);
            //return (hit.collider.gameObject.name == "Cover");
            return false;
            //return (hit.transform == this.transform);
        }
        else
            return true;
    }
}
