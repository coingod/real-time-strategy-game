﻿using UnityEngine;
using System.Collections;

public class Suppresion : MonoBehaviour
{
	public SuppresionIndicator indicatorPrefab;
	public float level = 100;
	public float rechargeFactor = 4;
    private float factor;
    public bool suppresed = false;
	
    private Transform _transform;
    private Unit lastSuppressedBy;
    private Unit owner;

    public Unit LastSuppressedBy
    {
        get
        {
            return lastSuppressedBy;
        }

        set
        {
            lastSuppressedBy = value;
        }
    }

    private void Start ()
    {
		_transform = transform;
        owner = transform.parent.GetComponent<Unit>();
        level = 100;
	}
	
	private void Update ()
    {
        factor = rechargeFactor;
        if (suppresed)
            factor = rechargeFactor * 2;

        if (level >= 0 && level < 100)
            level += Time.deltaTime * factor;
		else if(level >= 100)
        {
            level = 100;
            suppresed = false;
        }
	}
	
	public float GetPercentage()
    {
		return level / 100;
	}

	public virtual void Suppress(Unit attacker)
    {
        if (attacker.Player != owner.Player)
            lastSuppressedBy = attacker;

        Suppress(5);
    }

    public virtual void Suppress(float value)
    {
        if (level <= 0)
            return;

        level -= value;

        if (level <= 0)
        {
            suppresed = true;
            level = 0;
        }
    }
}

