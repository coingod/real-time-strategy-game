﻿using UnityEngine;
using System.Collections;

public class SuppresionIndicator : MonoBehaviour
{
	public Color happyColor = Color.green;
	public Color angryColor = Color.red;
	public float fadeTime = 4f;
	public Suppresion suppresiveUnit;
	
	Material _material;
	public Color _color;
	Quaternion _pointUpAtForward;
	
	private void Start ()
    {
		_material = GetComponent<Renderer>().material;

		UpdateColor();
	}
	
	private void Update ()
    {
		UpdateColor();

		//Fade out the graphic
		_color.a -= Time.deltaTime/fadeTime;
		_material.color = _color;
	}

	private void UpdateColor()
    {
		//Calculate the color
		var moodRatio = suppresiveUnit.level / 100;
		_material.color = _color = new Color(
			angryColor.r * (1 - moodRatio) + happyColor.r * moodRatio,
			angryColor.g * (1 - moodRatio) + happyColor.g * moodRatio,
			angryColor.b * (1 - moodRatio) + happyColor.b * moodRatio
			);
	}
}

