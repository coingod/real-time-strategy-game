﻿using UnityEngine;
using System.Collections;

public class PlayRandomDeadSound : MonoBehaviour
{
    private AudioSource audioSource;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void PlayGermanVO()
    {
        AudioClip[] clips = GameManager.Instance.ger_death;
        audioSource.clip = clips[Random.Range(0, clips.Length - 1)];
        audioSource.Play();
    }

    public void PlayRussianVO()
    {
        AudioClip[] clips = GameManager.Instance.rus_death;
        audioSource.clip = clips[Random.Range(0, clips.Length - 1)];
        audioSource.Play();
    }
}
