﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathNodeGizmo : MonoBehaviour {

    public Color color = Color.white;
    public float size = 1;
    public int F = 0;

    private TextMesh textmesh;

    public void Start()
    {
        textmesh = GetComponentInChildren<TextMesh>();
        textmesh.text = ""+F;
    }

    void OnDrawGizmos()
    {
        Gizmos.color = color;
        Gizmos.DrawWireSphere(transform.position, size * 0.5f);
        textmesh.color = color;
    }

}
