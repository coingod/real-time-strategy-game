﻿using UnityEngine;
using System.Collections;

public class CameraMovement : MonoBehaviour
{
	public float scrollWidth = 15;
	public float scrollSpeed = 25;
	public float minCameraHeight = 10;
	public float maxCameraHeight = 40;
	public float tiltSpeed = 30;

	private Quaternion originalRotation;
	private Vector3 originalPosition;

	private float lastTime = 0f;

	// Use this for initialization
	private void Start ()
    {
		originalRotation = transform.rotation;
		originalPosition = transform.position;
	}
	
	// Update is called once per frame
	private void Update ()
    {
		MoveCamera ();

		if (Input.GetButtonDown("Reset Camera"))
		{
			if (Time.time - lastTime < 0.2f)
			{
				lastTime = Time.time;
				transform.rotation = originalRotation;
                transform.position = new Vector3(transform.position.x, originalPosition.y, transform.position.z);
            }
            else
			{
				lastTime = Time.time;
			}
		}
	}

    public void CenterOnPoint(Vector3 point)
    {
        point.y = transform.position.y;
        point.z -= 6;
        transform.position = point;
    }

	private void MoveCamera()
    {
		float xpos = Input.mousePosition.x;
		float ypos = Input.mousePosition.y;
		Vector3 movement = Vector3.zero;
		Vector3 tilting = Vector3.zero;
		bool mouseScroll = false;
		
		//horizontal camera movement
		if((xpos >= 0 && xpos < scrollWidth) || IsPressingLeft())
        {
			movement.x -= scrollSpeed;
			//player.hud.SetCursorState(CursorState.PanLeft);
			mouseScroll = true;
		}
        else if((xpos <= Screen.width && xpos > Screen.width - scrollWidth) || IsPressingRight())
        {
			movement.x += scrollSpeed;
			//player.hud.SetCursorState(CursorState.PanRight);
			mouseScroll = true;
		}
		
		//vertical camera movement
		if((ypos >= 0 && ypos < scrollWidth) || IsPressingDown())
        {
			movement.z -= scrollSpeed;
			//player.hud.SetCursorState(CursorState.PanDown);
			mouseScroll = true;
		}
        else if((ypos <= Screen.height && ypos > Screen.height - scrollWidth) || IsPressingUp())
        {
			movement.z += scrollSpeed;
			//player.hud.SetCursorState(CursorState.PanUp);
			mouseScroll = true;
		}
		
		//make sure movement is in the direction the camera is pointing
		//but ignore the vertical tilt of the camera to get sensible scrolling
		movement = Camera.main.transform.TransformDirection(movement);
		movement.y = 0;

        //away from ground movement
        if (Input.GetKey(KeyCode.KeypadPlus))
        {
			movement.y -= scrollSpeed;
			tilting.y -= tiltSpeed;
		}
        else if (Input.GetKey(KeyCode.KeypadMinus))
		{
			movement.y += scrollSpeed;
			tilting.y += tiltSpeed;
		}

		//movement.y -= scrollSpeed * Input.GetAxis("Mouse ScrollWheel");
		//tilting.y -= tiltSpeed * Input.GetAxis("Mouse ScrollWheel");
		
		//calculate desired camera position based on received input
		Vector3 origin = Camera.main.transform.position;
		Vector3 destination = origin;
		destination.x += movement.x;
		destination.y += movement.y;
		destination.z += movement.z;
		
		//limit away from ground movement to be between a minimum and maximum distance
		if(destination.y > maxCameraHeight)
        {
			destination.y = maxCameraHeight;
		}
        else if(destination.y < minCameraHeight)
        {
			destination.y = minCameraHeight;
		}

		//calculate desired camera rotation based on received input
		Quaternion origin2 = transform.rotation;
		Quaternion destination2 = Quaternion.Euler(origin2.eulerAngles.x + tilting.y, 0, 0);//origin2;
		//destination2.x += movement.y;
		/*
		//limit away from ground movement to be between a minimum and maximum distance
		if(destination.y > maxCameraRotation) {
			destination2.x = maxCameraRotation;
		} else if(destination.y < minCameraRotation) {
			destination2.x = minCameraRotation;
		}
*/
		//if a change in position is detected perform the necessary update
		if(destination != origin)
        {
			transform.position = Vector3.MoveTowards(origin, destination, Time.deltaTime * scrollSpeed);
			transform.rotation = Quaternion.RotateTowards(origin2, destination2, Time.deltaTime * tiltSpeed);
		}
		
		//set cursor back to default state it should be in
		if(!mouseScroll)
        {
			//player.hud.SetCursorState(CursorState.Select);
		}
	}

	private bool IsPressingLeft()
    {
		return ( Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A) );
	}

	private bool IsPressingRight()
    {
		return ( Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D) );
	}

	private bool IsPressingDown()
    {
		return ( Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S) );
	}

	private bool IsPressingUp()
    {
		return ( Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W) );
	}
}
