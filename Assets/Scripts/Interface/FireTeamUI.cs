﻿using UnityEngine;
using UnityEngine.UI;

public class FireTeamUI : MonoBehaviour
{
    [SerializeField]
    private FireTeam fireTeam;

    private Button ftIconBtn;
    private Text ftType;
    private Text ftUnitCount;
    private Image ftCover;
    private Button ftHoldFireBtn;
    private Image ftUnderAttack;
    private Slider ftHealthSlider;
    private Slider ftSuppressionSlider;

    public FireTeam FireTeam
    {
        get
        {
            return fireTeam;
        }

        set
        {
            fireTeam = value;
        }
    }

    // Use this for initialization
    private void Start()
    {
        Transform icon = transform.Find("Icon");
        ftIconBtn = icon.GetComponent<Button>();
        ftIconBtn.onClick.AddListener(
            delegate ()
            {
                ShowUnits();
            }
        );//ShowUnits);

        ftType = icon.Find("Type").GetComponent<Text>();
        ftUnitCount = icon.Find("UnitCount").GetComponent<Text>();
        ftCover = icon.Find("Cover").GetComponent<Image>();
        ftHoldFireBtn = transform.Find("HoldFire").GetComponent<Button>();
        ftHoldFireBtn.onClick.AddListener(fireTeam.HoldFire);
        ftUnderAttack = icon.Find("UnderAttack").GetComponent<Image>();
        ftHealthSlider = transform.Find("Health").GetComponent<Slider>();
        ftSuppressionSlider = transform.Find("Suppresion").GetComponent<Slider>();

        ftType.text = fireTeam.name;
        ftUnitCount.text = fireTeam.maxUnits + "";
    }

    // Update is called once per frame
    private void Update()
    {
        if (!fireTeam)
            return;

        ftCover.enabled = fireTeam.leader.takingCover;
        ftUnderAttack.enabled = fireTeam.leader.attacking;

        ftHealthSlider.value = fireTeam.currentHealth;
        ftSuppressionSlider.value = fireTeam.currentSuppresion;
    }

    private void ShowUnits()
    {
        Transform units = transform.Find("Units");

        if (units.childCount > 0)
        {
            units.gameObject.SetActive(!units.gameObject.activeSelf);
        }
        else
        {
            float offset = 0;

            foreach (Unit u in fireTeam.units)
            {
                //Create Unit UI panel
                GameObject unit_ui = Instantiate(GameManager.Instance.unitUI);
                //unit_ui.transform.SetParent(fireTeam_ui.transform, false);
                unit_ui.transform.SetParent(units, false);

                //Setup it's position
                RectTransform unitRectTranform = unit_ui.GetComponent<RectTransform>();
                Vector2 anchoredPosition = unitRectTranform.anchoredPosition;
                unitRectTranform.anchoredPosition = new Vector2(anchoredPosition.x - offset, anchoredPosition.y);
                offset += unitRectTranform.rect.width;

                //Link UI with the Unit
                unit_ui.GetComponent<UnitUI>().Unit = u;
            }
        }
    }
}
