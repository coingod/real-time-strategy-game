using UnityEngine;
[ExecuteInEditMode]
public class PauseMenu : MonoBehaviour {
	/*
	//public GUISkin mySkin;
	public Texture2D header;
	
	private Player player;
	private string[] buttons = {"Resume", "Exit Game"};
	
	void Start () {
		player = transform.root.GetComponent<Player>();
	}
	
	void Update () {
		if(Input.GetKeyDown(KeyCode.Escape)) Resume();
	}
	
	void OnGUI() {
		//GUI.skin = mySkin;
		
		float groupLeft = Screen.width / 2 - GameManager.MenuWidth / 2;
		float groupTop = Screen.height / 2 - GameManager.PauseMenuHeight / 2;
		GUI.BeginGroup(new Rect(groupLeft, groupTop, GameManager.MenuWidth, GameManager.PauseMenuHeight));
		
		//background box
		GUI.Box(new Rect(0, 0, GameManager.MenuWidth, GameManager.PauseMenuHeight), "");
		//header image
		GUI.DrawTexture(new Rect(GameManager.Padding, GameManager.Padding, GameManager.HeaderWidth, GameManager.HeaderHeight), header);
		
		//menu buttons
		float leftPos = GameManager.MenuWidth / 2 - GameManager.ButtonWidth / 2;
		float topPos = 2 * GameManager.Padding + header.height/2;
		for(int i=0; i<buttons.Length; i++) {
			if(i > 0) topPos += GameManager.ButtonHeight + GameManager.Padding;
			if(GUI.Button(new Rect(leftPos, topPos, GameManager.ButtonWidth, GameManager.ButtonHeight), buttons[i])) {
				switch(buttons[i]) {
					case "Resume": Resume(); break;
					case "Exit Game": ExitGame(); break;
					default: break;
				}
			}
		}
		
		GUI.EndGroup();
	}
	
	private void Resume() {
		Time.timeScale = 1.0f;
		GetComponent<PauseMenu>().enabled = false;
		if(player) player.GetComponent<UserInput>().enabled = true;
		//Screen.showCursor = false;
		GameManager.MenuOpen = false;
	}
	
	private void ExitGame() {
		Application.Quit();
	}
    */
}