﻿using UnityEngine;
using System.Collections;

public class OrderRing : MonoBehaviour
{
    private float lifeTime = 3f;
	private Renderer renderer;

	// Use this for initialization
	private void Start ()
	{
		renderer = GetComponent<Renderer>();
		StartCoroutine ("Fade");
		Destroy(gameObject, lifeTime);
	}
	
	private IEnumerator Fade()
	{
		yield return new WaitForSeconds(1f);

		float t = 0.0f;
		float currentAlpha = renderer.material.color.a;
		Color thisColor;
		while(t <= 1)
		{
			thisColor = renderer.material.color;
			thisColor.a = Mathf.Lerp(currentAlpha, 0f, t);
			renderer.material.color = thisColor;
			t += Time.deltaTime/2;
			yield return null;
			
		}
		thisColor = renderer.material.color;
		thisColor.a = 0f;
		renderer.material.color = thisColor;
	}
	
}
