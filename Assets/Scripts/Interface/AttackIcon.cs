﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackIcon : MonoBehaviour
{
    private RectTransform rectTranform;
    // Use this for initialization
    void Start ()
    {
        rectTranform = GetComponent<RectTransform>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        rectTranform.anchoredPosition = Input.mousePosition;
        //Debug.Log(Input.mousePosition);
	}

}
