﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UnitUI : MonoBehaviour
{
    [SerializeField]
    private Unit unit;

    private Text unitName;
    private Text unitWeapon;
    private Text unitAmmo;
    private Text unitStatus;

    public Unit Unit
    {
        get
        {
            return unit;
        }

        set
        {
            unit = value;
        }
    }

    // Use this for initialization
    private void Start ()
    {
        unitName = transform.Find("Name").GetComponent<Text>();
        unitWeapon = transform.Find("Weapon").GetComponent<Text>();
        unitAmmo = transform.Find("Ammo").GetComponent<Text>();
        unitStatus = transform.Find("Status").GetComponent<Text>();
    }

    // Update is called once per frame
    private void Update ()
    {
        unitStatus.text = "Dead";

        if (!Unit)
            return;

        unitName.text = Unit.name;
        unitWeapon.text = Unit.GetCurrentWeapon().name;
        unitAmmo.text = Unit.GetCurrentWeapon().AmmoLeft() + "/" + Unit.GetCurrentWeapon().ammoPerClip;

        if (Unit.holdFire)
            unitStatus.text = "Holding Fire";
        else if (Unit.attacking)
            unitStatus.text = "Attacking";
        else if (Unit.reloading)
            unitStatus.text = "Reloading";
        else if (Unit.takingCover)
            unitStatus.text = "Taking Cover";
        else
            unitStatus.text = "Ready";
    }
}
