﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class HUD : MonoBehaviour
{
    public Canvas canvas;
    public GameObject FireTeamUI;
    public GameObject UnitUI;

	private Player player;
	private bool showRect;
	private Rect selectionRect;

	private bool showUnits = false;

	private float rightBardWidth = 160f;

	public void setSelectionBox(bool b, Rect rect) { showRect = b; selectionRect = rect; }

	private Vector2 scrollViewVector = Vector2.zero;

    private Vector2 defaultAnchoredPosition;

    // Use this for initialization
    void Start () {
		player = GetComponent<Player>();

        foreach (FireTeam fireTeam in player.GetAllFireTeams())
        {
            //Create FireTeam UI panel
            GameObject ui = Instantiate(FireTeamUI);
            ui.transform.SetParent(canvas.transform, false);

            //Link UI with the FireTeam
            ui.GetComponent<FireTeamUI>().FireTeam = fireTeam;

            //Disable by default
            ui.SetActive(false);

            //Set default anchored position for UI panels to reset it on redraw
            defaultAnchoredPosition = ui.GetComponent<RectTransform>().anchoredPosition;
        }
    }

    public void OnSelectionChange()
    {
        float offset = 0;
        foreach (Transform fireTeamUI in canvas.transform)
        {
			FireTeamUI fireTeam = fireTeamUI.GetComponent<FireTeamUI>();

			if (!fireTeam)
			{
				continue;
			}

			//FireTeam selected?
			if (player.selectedTeams.Contains(fireTeam.FireTeam))
            {
                //Show the UI Panel
                fireTeamUI.gameObject.SetActive(true);
                //Setup it's position
                RectTransform fireTeamRectTranform = fireTeamUI.GetComponent<RectTransform>();
                Vector2 anchoredPosition = defaultAnchoredPosition;//fireTeamRectTranform.anchoredPosition;
                fireTeamRectTranform.anchoredPosition = new Vector2(anchoredPosition.x, anchoredPosition.y - offset);
                offset += fireTeamRectTranform.rect.height;
            }
            else
                fireTeamUI.gameObject.SetActive(false);
        }
	}

	void OnGUI() {
		if (showRect)
		{
			GUI.Box(selectionRect, "");
		}

        /*
		if(player.selectionList.Count > 0) {


			scrollViewVector = GUI.BeginScrollView(new Rect (Screen.width - rightBardWidth, 25, rightBardWidth, Screen.height-50), scrollViewVector, new Rect(0,0,100,2*Screen.height));
			float pos = 0;
			foreach(FireTeam u in player.selectedTeams) {
                pos = DrawFireTeamPanel(u, pos);

            }
			
			GUI.EndScrollView();
		}
        */
	}

	public float DrawFireTeamPanel(FireTeam u, float pos) {
		GUI.Box(new Rect(0, pos, rightBardWidth-20, 170), u.name);
		pos += 20;
		GUI.Button (new Rect (10, pos, rightBardWidth-40, 80), u.icon);
		pos += 80 + 10;
		GUI.Label (new Rect (10, pos, 80, 20), TeamStatus(u));
		pos += 20;
		GUI.Label (new Rect (10, pos, 80, 20), TeamTarget(u));
		pos += 20;
		if (GUI.Button (new Rect (10, pos, 80, 20), "Hold Fire"))
			foreach(Unit x in u.units) {
				//holdFire = !holdFire;
				x.holdFire = !x.holdFire;
			}
		pos += 20;
		if(GUI.Button (new Rect (10, pos, 80, 20), "Show Units")) 
			showUnits = !showUnits;
		pos += 30;
		if(showUnits)
		foreach(Unit x in u.units)
			if(player.GetSelectedUnits().Contains(x)) pos = DrawUnitPanel(x,pos);
		//pos += 20;
		//findCover = GUI.Toggle (new Rect (10, pos, 80, 20), u.findCover, "Find Cover");
		//pos += 20;
		/*
		GUI.Label (new Rect (10, pos, 80, 20), "Wp: "+u.currentWeapon.name);
		pos += 20;
		GUI.Label (new Rect (10, pos, 80, 20), "A: "+u.currentWeapon.ammoLeftInClip+"/"+u.currentWeapon.ammoPerClip);
		pos += 20;
		*/
		return pos + 10;
	}

	public float DrawUnitPanel(Unit u, float pos) {
		GUI.Box(new Rect(0, pos, rightBardWidth-20, 60), u.name);
		pos += 20;
		GUI.Label (new Rect (10, pos, 80, 20), "Wp: "+u.GetCurrentWeapon().name);
		pos += 20;
		GUI.Label (new Rect (10, pos, 80, 20), "A: "+u.GetCurrentWeapon().AmmoLeft() + "/"+u.GetCurrentWeapon().ammoPerClip);
		pos += 20;
		return pos + 10;
	}

	public string TeamStatus(FireTeam u) {
		if(!u.leader.GetCurrentWeapon().HasAmmo()) return "Reloading!";
		if(u.leader.moving) return "Moving!";
		if(u.leader.attacking) return "Attacking!";
		else return "Awaiting";
	}

	public string TeamTarget(FireTeam u) {
		Transform trg = u.leader.target;
		if(trg){
			Unit tu = trg.GetComponent<Unit>();
			if(tu) {
				return "Trg: "+tu.name;
			}
		}
		return "No target";

	}


	public bool MouseInBounds() {
		//Screen coordinates start in the lower-left corner of the screen
		//not the top-right of the screen like the drawing coordinates do
		Vector3 mousePos = Input.mousePosition;
		bool insideWidth = mousePos.x >= 0 && mousePos.x <= Screen.width - rightBardWidth;
		//bool insideHeight = mousePos.y >=0 && mousePos.y <= Screen.height - RESOURCE_BAR_HEIGHT;
		return insideWidth ;//&& insideHeight;
	}
}
