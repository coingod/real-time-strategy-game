using System.Collections.Generic;

public static class UnitManager
{
	private static List<Player> players;

	public static void AddPlayer(Player player)
	{
		if(players == null)
		{
			players = new List<Player>();
		}
		players.Add(player);
	}

	public static int GetPlayersCount()
	{
		return players.Count;
	}

	public static Player GetPlayer(int playerId)
	{
		return players[playerId];
	}

	public static bool AIControlled(Player player)
	{
		return player.Human;
	}

	public static bool AIControlled(int playerId)
	{
		return players[playerId].Human;
	}

	/*
	public static int GetUnitsCount(Player p) {
		return p.GetUnitsCount();
	}

	public static int GetUnitsCount(int p) {
		return players[p].GetUnitsCount();
	}
	*/

	public static Unit GetUnit(Player player, int fireTeamId, int unitId)
	{
		return player.GetFireTeam(fireTeamId).GetUnit(unitId);
	}

	public static Unit GetUnit(int playerId, int fireTeamId, int unitId)
	{
		return players[playerId].GetFireTeam(fireTeamId).GetUnit(unitId);
	}

	public static string PlayersToString()
	{
		string s = "";
		foreach(Player p in players)
			s+=p.ToString()+" ";
		return s;
	}
}
