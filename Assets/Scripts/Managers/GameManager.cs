using UnityEngine;
using System.Collections.Generic;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;                     //Static reference to this Class

    [Header("Game Settings")]
    public bool coverFromTarget = false;                    //If TRUE Units will try to find better Cover positions (More costly)

    [Header("Unit Resources")]
    public GameObject orderRing;					        //The ring effect to spawn in the target position
    public GameObject selectionRing;                        //The Ring prefab that displays if this unit is currently selected
    public GameObject ragdoll;                              //The unit's dead body ragdoll
    public GameObject bloodSplats;                          //Unit's blood pool when dead
    public GameObject explosionDecal;                       //Mark on the ground that a explosion leaves

    [Header("UI Resources")]
    public GameObject fireTeamUI;
    public GameObject unitUI;
    public GameObject attackIcon;

    [Header("German Voices")]
    public bool useRO2Voices = false;
    public AudioClip[] ger_enemyDown;
    public AudioClip[] ger_reloading;
    public AudioClip[] ger_attack;
    public AudioClip[] ger_move;
    public AudioClip[] ger_grenade;
    public AudioClip[] ger_enemyMg;
    public AudioClip[] ger_takeCover;
    public AudioClip[] ger_onCombat;
    public AudioClip[] ger_death;

    [Header("Russian Voices")]
    public AudioClip[] rus_enemyDown;
    public AudioClip[] rus_reloading;
    public AudioClip[] rus_attack;
    public AudioClip[] rus_move;
    public AudioClip[] rus_grenade;
    public AudioClip[] rus_enemyMg;
    public AudioClip[] rus_takeCover;
    public AudioClip[] rus_onCombat;
    public AudioClip[] rus_death;

    [Header("Weapon Resources")]
    public GameObject bulletHitSmokeFX;                         //The bullet impact Smoke particle effect
    public GameObject bulletHitBloodFX;                         //The bullet impact Blood particle effect

    [Header("Debug")]
    public bool DebugPathfinding = false;
    public GameObject PathNodeGizmo;

    private void Awake()
    {
        //This is a common approach to handling a class with a reference to itself.
        //If instance variable doesn't exist, assign this object to it
        if (Instance == null)
            Instance = this;
        //Otherwise, if the instance variable does exist, but it isn't this object, destroy this object.
        //This is useful so that we cannot have more than one GameManager object in a scene at a time.
        else if (Instance != this)
            Destroy(this);
    }

    private void Start()
    {
        Application.targetFrameRate = -1;

        if (useRO2Voices)
            LoadCharacterVoices();
    }

    private void LoadCharacterVoices()
    {
        Object[] aud = Resources.LoadAll("AUD_VOX_Chatter_GerEng_01");

        List<AudioClip> ger_move_list = new List<AudioClip>();
        List<AudioClip> ger_enemyDown_list = new List<AudioClip>();
        List<AudioClip> ger_reloading_list = new List<AudioClip>();
        List<AudioClip> ger_grenade_list = new List<AudioClip>();
        List<AudioClip> ger_enemyMg_list = new List<AudioClip>();
        List<AudioClip> ger_takeCover_list = new List<AudioClip>();
        List<AudioClip> ger_attack_list = new List<AudioClip>();
        List<AudioClip> ger_onCombat_list = new List<AudioClip>();
        List<AudioClip> ger_death_list = new List<AudioClip>();

        foreach (AudioClip clip in aud)
        {
            if (clip.name.Contains("ForceRespawn") || clip.name.Contains("MoveTO") || clip.name.Contains("FollowMe"))
                ger_move_list.Add(clip);
            else if (clip.name.Contains("Reloading"))
                ger_reloading_list.Add(clip);
            else if (clip.name.Contains("EnemyDeath_L") || clip.name.Contains("EnemyDeath_N"))
                ger_enemyDown_list.Add(clip);
            else if (clip.name.Contains("ThrowingGrenade"))
                ger_grenade_list.Add(clip);
            else if (clip.name.Contains("TakingFireMG"))
                ger_enemyMg_list.Add(clip);
            else if (clip.name.Contains("TakingFireGeneric"))
                ger_takeCover_list.Add(clip);
            else if (clip.name.Contains("SuppressingFire") || clip.name.Contains("InfantrySpotted"))
                ger_attack_list.Add(clip);
            else if (clip.name.Contains("AttackGeneric"))
                ger_onCombat_list.Add(clip);
            else if (clip.name.Contains("DyingFast"))
                ger_death_list.Add(clip);
        }

        ger_move = ger_move_list.ToArray();
        ger_enemyDown = ger_enemyDown_list.ToArray();
        ger_reloading = ger_reloading_list.ToArray();
        ger_grenade = ger_grenade_list.ToArray();
        ger_enemyMg = ger_enemyMg_list.ToArray();
        ger_takeCover = ger_takeCover_list.ToArray();
        ger_attack = ger_attack_list.ToArray();
        ger_onCombat = ger_onCombat_list.ToArray();
        ger_death = ger_death_list.ToArray();

        Resources.UnloadUnusedAssets();

        aud = Resources.LoadAll("AUD_VOX_Chatter_RusEng_01");

        List<AudioClip> rus_move_list = new List<AudioClip>();
        List<AudioClip> rus_enemyDown_list = new List<AudioClip>();
        List<AudioClip> rus_reloading_list = new List<AudioClip>();
        List<AudioClip> rus_grenade_list = new List<AudioClip>();
        List<AudioClip> rus_enemyMg_list = new List<AudioClip>();
        List<AudioClip> rus_takeCover_list = new List<AudioClip>();
        List<AudioClip> rus_attack_list = new List<AudioClip>();
        List<AudioClip> rus_onCombat_list = new List<AudioClip>();
        List<AudioClip> rus_death_list = new List<AudioClip>();

        foreach (AudioClip clip in aud)
        {
            if (clip.name.Contains("ForceRespawn") || clip.name.Contains("MoveTO") || clip.name.Contains("FollowMe"))
                rus_move_list.Add(clip);
            else if (clip.name.Contains("Reloading"))
                rus_reloading_list.Add(clip);
            else if (clip.name.Contains("EnemyDeath_L") || clip.name.Contains("EnemyDeath_N"))
                rus_enemyDown_list.Add(clip);
            else if (clip.name.Contains("ThrowingGrenade"))
                rus_grenade_list.Add(clip);
            else if (clip.name.Contains("TakingFireMG"))
                rus_enemyMg_list.Add(clip);
            else if (clip.name.Contains("TakingFireGeneric"))
                rus_takeCover_list.Add(clip);
            else if (clip.name.Contains("SuppressingFire") || clip.name.Contains("InfantrySpotted"))
                rus_attack_list.Add(clip);
            else if (clip.name.Contains("AttackGeneric"))
                rus_onCombat_list.Add(clip);
            else if (clip.name.Contains("DyingFast"))
                rus_death_list.Add(clip);
        }

        rus_move = rus_move_list.ToArray();
        rus_enemyDown = rus_enemyDown_list.ToArray();
        rus_reloading = rus_reloading_list.ToArray();
        rus_grenade = rus_grenade_list.ToArray();
        rus_enemyMg = rus_enemyMg_list.ToArray();
        rus_takeCover = rus_takeCover_list.ToArray();
        rus_attack = rus_attack_list.ToArray();
        rus_onCombat = rus_onCombat_list.ToArray();
        rus_death = rus_death_list.ToArray();

        Resources.UnloadUnusedAssets();
    }
}