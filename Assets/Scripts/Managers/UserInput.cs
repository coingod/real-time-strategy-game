﻿using UnityEngine;
using System.Collections.Generic;

[RequireComponent (typeof (HUD))]
[RequireComponent (typeof (Player))]
public class UserInput : MonoBehaviour
{ 
	private Player player;                                  //The current active human Player
    private HUD hud;                                        //The User Interface 
    private bool selecting;                                 //Are we currently selecting Units?
    private Vector3 oldMouse;                               //Position of the cursor on the previous frame
    private Vector3 selectionStart;                         //Start position of the selection box 
    public Rect selectionBox;                               //The Selection Box

    public KeyCode lastPressedKey = KeyCode.A;
    public float keyCoolDown = 0.5f;

    private GameObject lastHoverObject;


    // Use this for initialization
    private void Start ()
    {
		selectionBox = new Rect();
		player = GetComponent<Player>();
		hud = GetComponent <HUD>();
	}
	
	// Update is called once per frame
	private void Update ()
    {
        /*
		if(Input.GetKeyDown(KeyCode.Escape))
            OpenPauseMenu();
        */
		if(player)
        {
			MouseActivity();

            KeyboardActivity();

            //selection box
            if (Input.GetMouseButtonDown(0))
			{
				selecting = true;
				selectionStart = Input.mousePosition;
			}
			else if (Input.GetMouseButtonUp(0))
			{
				selecting = false;
				selectionBox = Rect.MinMaxRect(0, 0, 0, 0);
			}
			
			//Update the selected units if the mouse has moved
			if (selecting && oldMouse != Input.mousePosition)
			{
				UpdateBoxSelection(selectionStart, Input.mousePosition);
			}
			oldMouse = Input.mousePosition;

			//draw box
			hud.setSelectionBox(selecting, selectionBox);
		}

        if (keyCoolDown > 0)
            keyCoolDown -= Time.deltaTime;
        else
            keyCoolDown = 0;
    }
	
	private void UpdateBoxSelection(Vector3 from, Vector3 to)
	{
		RaycastHit hit1, hit2;
		Ray mouseRay1 = Camera.main.ScreenPointToRay(from);
		Ray mouseRay2 = Camera.main.ScreenPointToRay(to);
		if (Physics.Raycast(mouseRay1, out hit1, 1000) && Physics.Raycast(mouseRay2, out hit2, 1000))
		{
			//draw box info
			float minx = Mathf.Min(from.x, to.x);
			float miny = Mathf.Min(from.y, to.y);
			float maxx = Mathf.Max(from.x, to.x);
			float maxy = Mathf.Max(from.y, to.y);
			float w = maxx - minx;
			float h = maxy - miny;
			//not double click?
			if (w != Screen.width || h != Screen.height)
			{
				selectionBox = Rect.MinMaxRect(minx, Screen.height - maxy, maxx, Screen.height - miny);
			}
			//selection
			player.BoxSelection(hit1.point, hit2.point);
			
		}
	}

	private void OpenPauseMenu()
    {
		Time.timeScale = 0.0f;
		GetComponentInChildren<PauseMenu>().enabled = true;
		GetComponent<UserInput>().enabled = false;
		//Screen.showCursor = true;
		//GameManager.MenuOpen = true;
	}

    private void KeyboardActivity()
    {
        //For testing purposes only
        if (!Input.anyKeyDown)
            return;

        FireTeam[] fts = player.GetAllFireTeams().ToArray();
        int index = 0;
        foreach (FireTeam ft in fts)
        {
            KeyCode key = (KeyCode) 49 + index++;// Alpha1 = 49
            if (Input.GetKeyDown(key))
            {
                player.ResetSelection();
                foreach (Unit u in ft.units)
                    player.AddSelection(u);

                if (keyCoolDown > 0 && lastPressedKey == key)
                    Camera.main.GetComponent<CameraMovement>().CenterOnPoint(ft.FireTeamPosition());
                lastPressedKey = key;
                keyCoolDown = 0.5f;
            }
        }
    }

    private void MouseActivity()
    {
		if(Input.GetMouseButtonDown(0))
            LeftMouseClick();
		else if(Input.GetMouseButtonDown(1))
            RightMouseClick();

		MouseHover();
	}
	
	private void LeftMouseClick()
    {
		if(!hud.MouseInBounds())
			return;
		GameObject hitObject = FindHitObject(Input.mousePosition);
		Vector3 hitPoint = FindHitPoint(Input.mousePosition);
		Vector3 mierda = new Vector3 (9999, 9999, 9999);
		if(hitObject && hitPoint != mierda) {
			//Debug.Log ("LeftClicked " + hitObject.name);
			if(player.GetSelectedUnits().Count > 0)
                SelectionMouseClick(hitObject, hitPoint);
			else if(hitObject.name != "Ground")
            {
				Unit unit = hitObject.transform.GetComponent<Unit>();
				if(unit)
                {
                    //we already know the player has no selected object
                    if (player.OwnerOfUnit(unit))
                        player.AddSelection(unit);
				}
			}
		}
	}

	private void SelectionMouseClick(GameObject hitObject, Vector3 hitPoint)
    {
		/*
		foreach(Unit u in player.selectionList) {
			u.targetPosition = hitPoint;
			u.SearchPath ();
		}
		*/
		int count = 0;
		float delta = 1.5f;
		int unitsPerRow = 3;
		
		//find ref pos, avg. of all selected units pos.
		//RefPos = (pos1 + ... + posN)/N
		Vector3 refPos = Vector3.zero;
		foreach (Unit u in player.GetSelectedUnits())
		{
			refPos += u.transform.position;//TODO Optimize with GetPos in Unit
		}
		refPos /= player.GetSelectedUnits().Count; //TODO Optimize with get selection count
		
		//find where to send them
		List<Unit> tunits = new List<Unit>();
		List<Vector3> posList = new List<Vector3>();
		Vector3 dir = refPos - hitPoint;//Vector from refPos to TargetPos(hitpoint)
		Quaternion q = Quaternion.LookRotation(dir);//Creates a rotation with dir
		float x0 = player.GetSelectedUnits().Count < unitsPerRow ? (0.5f * (player.GetSelectedUnits().Count - 1)) * -delta : 1 * -delta;
		foreach (Unit u in player.GetSelectedUnits())
		{
			float x = x0 + (count % unitsPerRow) * delta;
			float z = (count / unitsPerRow) * delta;
			Vector3 add = new Vector3(x, 0, z);
			Vector3 ntrg = hitPoint + q * add;
			posList.Add(ntrg);
			tunits.Add(u);
			count++;
		}
		
		for (int i = 0; i < posList.Count; i++)
		{
			//Sends the closest unit first, then the next and so on
			//Avoids Units to cross each other in their path
			int tosend = -1;
			float closest = 999999;
			for (int j = 0; j < tunits.Count; j++)
			{
				if (tunits[j] != null)
				{
					float dist = DistanceSqr(posList[i], tunits[j].transform.position);
					if (dist < closest)
					{
						closest = dist;
						tosend = j;
					}
				}
			}
			//send
			Unit mu = tunits[tosend];
			//mu.targetPosition = posList[i];
			mu.MouseClick (hitObject, posList[i]);
			//remove from list
			tunits[tosend] = null;
		}
	}
	
	private float DistanceSqr(Vector3 from, Vector3 to)
	{
		return ((from.x - to.x) * (from.x - to.x) + (from.y - to.y) * (from.y - to.y) + (from.z - to.z) * (from.z - to.z));
	}
	
	private void RightMouseClick()
    {
		if(!hud.MouseInBounds())
			return;
		if(player.GetSelectedUnits().Count > 0)
        {//&& player.SelectedObject) {
			//Debug.Log ("RightClicked");
			player.ResetSelection();
		}
	}
	
	private void MouseHover()
    {
		GameObject hoverObject = FindHitObject(Input.mousePosition);
		if(hoverObject && hoverObject != lastHoverObject)
        {
            //GameManager.Instance.attackIcon.SetActive(false);
            //if(player.SelectedObject) player.SelectedObject.SetHoverState(hoverObject);
            if (hoverObject.tag != "Ground")
            {
				//Debug.Log("MouseHover "+hoverObject.name);
				Player owner = hoverObject.transform.root.GetComponent<Player>();
				if(owner) {
                    if (owner != player)
                        GameManager.Instance.attackIcon.SetActive(true);
				}
			}
		}
	}

	public GameObject FindHitObject(Vector3 origin)
    {
		Ray ray = Camera.main.ScreenPointToRay(origin);
		RaycastHit hit;
		if(Physics.Raycast(ray, out hit)) return hit.collider.gameObject;
		return null;
	}
	
	public Vector3 FindHitPoint(Vector3 origin)
    {
		Ray ray = Camera.main.ScreenPointToRay(origin);
		RaycastHit hit;
		if(Physics.Raycast(ray, out hit)) return hit.point;
		return new Vector3(9999,9999,9999);// GameManager.InvalidPosition;
	}
}