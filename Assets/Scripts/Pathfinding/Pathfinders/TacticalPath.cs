//#define ASTAR_MORE_PATH_IDS
//#define ASTAR_NO_LOGGING //Disables path error logging totally. This also reduces memory allocations because the logging strings will not be allocated. It does not affect normal logging calls, only error calls since they are allocated even though they are not logged
//#define ASTAR_LOCK_FREE_PATH_STATE
//#define ASTARDEBUG

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Pathfinding;

namespace Pathfinding
{
	
	public class TacticalPath : ABPath
    {
        /*
        [System.Obsolete("Use PathPool<T>.GetPath instead")]
        public TacticalPath(Vector3 start, Vector3 end, OnPathDelegate callbackDelegate)
        {
            Reset();
            Setup(start, end, callbackDelegate);
        }
        */
        public static new TacticalPath Construct (Vector3 start, Vector3 end, OnPathDelegate callback = null)
        {
            TacticalPath p = PathPool<TacticalPath>.GetPath ();
			p.Setup (start, end, callback);
			return p;
		}

        public override void Initialize()
        {

            // Mark nodes to enable special connection costs for start and end nodes
            // See GetConnectionSpecialCost
            if (startNode != null) pathHandler.GetPathNode(startNode).flag2 = true;
            if (endNode != null) pathHandler.GetPathNode(endNode).flag2 = true;

            if (hasEndPoint && startNode == endNode)
            {
                PathNode endNodeR = pathHandler.GetPathNode(endNode);
                endNodeR.node = endNode;
                endNodeR.parent = null;
                endNodeR.H = 0;
                endNodeR.G = 0;
                Trace(endNodeR);
                CompleteState = PathCompleteState.Complete;
                return;
            }

            //Adjust the costs for the end node
            /*if (hasEndPoint && recalcStartEndCosts) {
				endNodeCosts = endNode.InitialOpen (open,hTarget,(Int3)endPoint,this,false);
				callback += ResetCosts; /* \todo Might interfere with other paths since other paths might be calculated before #callback is called *
			}*/

            PathNode startRNode = pathHandler.GetPathNode(startNode);
            startRNode.node = startNode;
            startRNode.pathID = pathHandler.PathID;
            startRNode.parent = null;
            startRNode.cost = 0;
            startRNode.G = GetTraversalCost(startNode);
            startRNode.H = CalculateHScore(startNode);

            /*if (recalcStartEndCosts) {
				startNode.InitialOpen (open,hTarget,startIntPoint,this,true);
			} else {*/
            startNode.Open(this, startRNode, pathHandler);
            //}

            searchedNodes++;

            partialBestTarget = startRNode;

            //any nodes left to search?
            if (pathHandler.HeapEmpty())
            {
                if (calculatePartial)
                {
                    CompleteState = PathCompleteState.Partial;
                    Trace(partialBestTarget);
                }
                else
                {
                    Error();
                    LogError("No open points, the start node didn't open any nodes");
                    return;
                }
            }

            currentR = pathHandler.PopNode();
        }

        public override void CalculateStep(long targetTick)
        {

            int counter = 0;

            //Continue to search while there hasn't ocurred an error and the end hasn't been found
            while (CompleteState == PathCompleteState.NotCalculated)
            {

                searchedNodes++;

                //Close the current node, if the current node is the target node then the path is finished
                if (currentR.node == endNode)
                {
                    CompleteState = PathCompleteState.Complete;
                    break;
                }

                if (currentR.H < partialBestTarget.H)
                {
                    partialBestTarget = currentR;
                }

                AstarProfiler.StartFastProfile(4);
                //Debug.DrawRay ((Vector3)currentR.node.Position, Vector3.up*2,Color.red);

                //Loop through all walkable neighbours of the node and add them to the open list.
                currentR.node.Open(this, currentR, pathHandler);

                AstarProfiler.EndFastProfile(4);

                //any nodes left to search?
                if (pathHandler.HeapEmpty())
                {
                    Error();
                    LogError("No open points, whole area searched");
                    return;
                }


                //Select the node with the lowest F score and remove it from the open list
                AstarProfiler.StartFastProfile(7);
                currentR = pathHandler.PopNode();
                AstarProfiler.EndFastProfile(7);
                DebugNode(currentR.node);

                //Check for time every 500 nodes, roughly every 0.5 ms usually
                if (counter > 500)
                {

                    //Have we exceded the maxFrameTime, if so we should wait one frame before continuing the search since we don't want the game to lag
                    if (System.DateTime.UtcNow.Ticks >= targetTick)
                    {
                        //Return instead of yield'ing, a separate function handles the yield (CalculatePaths)
                        return;
                    }
                    counter = 0;

                    if (searchedNodes > 1000000)
                    {
                        throw new System.Exception("Probable infinite loop. Over 1,000,000 nodes searched");
                    }
                }

                counter++;
            }


            AstarProfiler.StartProfile("Trace");

            if (CompleteState == PathCompleteState.Complete)
            {
                Trace(currentR);
            }
            else if (calculatePartial && partialBestTarget != null)
            {
                CompleteState = PathCompleteState.Partial;
                Trace(partialBestTarget);
            }

            AstarProfiler.EndProfile();

        }

        protected override void Recycle()
        {
            PathPool<TacticalPath>.Recycle(this);
        }

        public override uint GetTraversalCost(GraphNode node)
        {
            GetHostileFireCost(node);
            unchecked { return GetTagPenalty((int)node.Tag) + node.Penalty; }
        }

        public uint GetHostileFireCost(GraphNode node)
        {
            List<FireTeam> fts = UnitManager.GetPlayer(0).GetAllFireTeams();
            Vector3 position = (Vector3)node.position;
            RaycastHit hit;
            int blockMask = ((1 << 10));// | (1 << 8));
            node.Penalty = 0;
            float distance;
            //Debug.Log(position);
            foreach (FireTeam f in fts)
            {
                distance = Vector3.Distance(position, f.transform.position);
                if (distance < 25f && !Physics.Raycast(position + Vector3.up, f.transform.position - position, out hit, distance, blockMask))
                {
                    node.Penalty += 5000;

                    if (GameManager.Instance.DebugPathfinding) Debug.DrawRay(position + Vector3.up, f.transform.position - position, Color.red, distance);
                }
            }
            return 0;
        }


        private void DebugNode(GraphNode node)
        {
            if (!GameManager.Instance.DebugPathfinding)
                return;
            GameObject currentDebugNode = Object.Instantiate(GameManager.Instance.PathNodeGizmo, (Vector3)node.position, Quaternion.identity);
            PathNodeGizmo gizmo = currentDebugNode.GetComponent<PathNodeGizmo>();
            gizmo.F = (int) pathHandler.GetPathNode(node).F;

            if (node.Penalty > 0)
                gizmo.color = Color.red;
        }
        
    }
}