﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof (Unit))]
public class UnitSight : MonoBehaviour
{
	/** The Unit this AI belongs to */
	private Unit owner;
	
	/** The angle of vision, field of view */
	public float viewAngle = 180f;

	/** How far the Unit can see */
	public float viewRange = 50f;

	/** How far the Unit can hear */
	public float hearRange = 10f;

	/** Gameobjects that can block the unit view */
	public LayerMask blocksVision;
	
	/** Last known position the enemy was seen */
	private Vector3 enemyLastSeen;

    private float d = 0;
	
	// Use this for initialization
	private void Start ()
	{
		owner = GetComponent<Unit>();
	}
	
	// Update is called once per frame
	private void Update ()
	{
        //check for a target if no target
        if (!owner.target)
        {
            UpdateTarget();
        }
        else if (!CanSeeTarget(owner.target, ref d))
            owner.target = null;
	}

    protected void UpdateTarget()
    {		
		int mid = -1;
		int mplayer = -1;
		int msquad = -1;
		for (int j = 0; j < UnitManager.GetPlayersCount(); j++)
		{
			//If this player is the unit owner
			if(UnitManager.GetPlayer(j) == owner.Owner()) continue;
				
			for (int s = 0; s < UnitManager.GetPlayer(j).GetFireTeamsCount(); s++)
			{
				for (int i = 0; i < UnitManager.GetPlayer(j).GetFireTeam(s).GetUnitsCount(); i++) {
					Unit enemy = UnitManager.GetUnit(j, s, i);

					if (CanSeeTarget(enemy.transform, ref d))
					{
						mid = i;
						msquad = s;
						mplayer = j;
					}
				}
			}
		}
			
		//if has an enemy target
		if (mid != -1)
		{
			owner.target = UnitManager.GetUnit(mplayer, msquad, mid).transform;
			return;
		}
		//check any attacker
        /*
		if (owner.LastDamagedBy)
		{
			owner.target = owner.LastDamagedBy.transform;
            owner.LastDamagedBy = null;
        }
        else*/ if (owner.suppresion.LastSuppressedBy)
        {
            owner.target = owner.suppresion.LastSuppressedBy.transform;
            owner.suppresion.LastSuppressedBy = null;
        }
    }

	// Checks if target is visible, within field of view, or close enough to be heard
	protected bool CanSeeTarget (Transform target, ref float distance)
	{
		bool canSee = false;
		float targetAngle = Vector3.Angle(target.position - transform.position, transform.forward);
		float targetDistance = Vector3.Distance(transform.position, target.position);

		//is target within range and angle
		if(targetDistance < viewRange && Mathf.Abs(targetAngle) < viewAngle/2)
        {
			if(!Physics.Linecast(transform.position + Vector3.up, target.position + Vector3.up, blocksVision))
            {
				enemyLastSeen = target.position;
				canSee=true;
				//visitedLastSeen = false;
			}
		}

		//is target close enough to hear?
		if(targetDistance < hearRange)
        {
			enemyLastSeen = target.position;
			canSee=true;
			//visitedLastSeen = false;
		}

		distance = targetDistance;
		return canSee;
	}
	
	private void OnDrawGizmosSelected ()
	{
		Gizmos.color = Color.green;
		
		//Draw field of view
		Quaternion leftRayRotation = Quaternion.AngleAxis( -viewAngle/2, Vector3.up );
		Quaternion rightRayRotation = Quaternion.AngleAxis( viewAngle/2, Vector3.up );
		
		Vector3 leftRayDirection = leftRayRotation * transform.forward;
		Vector3 rightRayDirection = rightRayRotation * transform.forward;
		
		Gizmos.DrawRay( transform.position, leftRayDirection * viewRange );
		Gizmos.DrawRay( transform.position, rightRayDirection * viewRange );
		
		Gizmos.color = Color.red;
		Gizmos.DrawWireSphere(transform.position, viewRange);
		
		Gizmos.color = Color.yellow;
		Gizmos.DrawWireSphere(transform.position, hearRange);
	}
}

