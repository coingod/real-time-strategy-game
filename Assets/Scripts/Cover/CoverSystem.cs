﻿using UnityEngine;
using System.Collections.Generic;

public class CoverSystem : MonoBehaviour
{
	public List<CoverNode> coverNodes;
	public bool debugCover = false;
	public LayerMask coverLayer = 1 << 8;//Layer 8, Cover;

	// Use this for initialization
	private void Start ()
    {
		//Store all cover nodes in the list
		foreach( Transform child in transform.GetChild(0) )
			coverNodes.Add(child.GetComponent<CoverNode>());
	}

	public CoverNode FindClosestCoverNode( Vector3 pos )
    {
		List<CoverNode> goodCoverNodes = new List<CoverNode>();
		foreach(CoverNode cn in coverNodes)
        {
			if(!cn.inUse)
            {
                if(GoodCover(cn, pos))
                {
                    if (!GameManager.Instance.coverFromTarget)
                        goodCoverNodes.Add(cn);
                    else if (GoodCoverFromTarget(cn))
                            goodCoverNodes.Add(cn);
                }
				//if(coverFromTarget) {
					//if(GoodCover(cn, pos)) {
					/*
					if(GoodCoverFromTarget(cn)) 
                    {
						goodCoverNodes.Add(cn);
					}
					*/
				//}else{
                /*
					if(GoodCover(cn, pos))
                    {
						goodCoverNodes.Add(cn);
					}
                */
				//}
			}
		}
		if(goodCoverNodes.Count > 0)
        {
			CoverNode closest = goodCoverNodes[0];
			Vector3 closestPos = closest.transform.position;
			foreach(CoverNode c in goodCoverNodes)
            {
				if(Vector3.Distance(pos,c.transform.position) < Vector3.Distance(pos,closestPos))
                {
					closest = c;
					closestPos = closest.transform.position;
				}
			}
			return closest;
		}
		return null;
	}
	
	public bool GoodCoverFromTarget(CoverNode cn) 
    {
		Vector3 cNodePos = cn.transform.position;
		RaycastHit hit;
		bool good = true;
		Player enemy = UnitManager.GetPlayer(0);
        int j = 0; int i = 0;
        while (j < enemy.GetFireTeamsCount() && good)
        {
            while (i < enemy.GetFireTeam(j).GetUnitsCount() && good)
            {
                Vector3 targetPos = UnitManager.GetUnit(0, j, i).transform.position;
                if (debugCover) Debug.DrawRay(targetPos + Vector3.up, cNodePos - targetPos, Color.red, 5.0f);

                //If the Ray doesn't hit something on the way, we can be seen!
                if (!Physics.Raycast(targetPos + Vector3.up, cNodePos - targetPos, out hit, Vector3.Distance(targetPos, cNodePos), coverLayer.value))
                    good = false;

                i++;
            }
            j++;
        }
        /*
		for(int j = 0; j < enemy.GetFireTeamsCount(); j++) 
        {
			//TODO Filter visible Units!
			for(int i = 0; i < enemy.GetFireTeam(j).GetUnitsCount(); i++) 
            {
				//Vector3 targetPos = t.transform.position;
				Vector3 targetPos = UnitManager.GetUnit(0,j,i).transform.position;
				if(debugCover) Debug.DrawRay(targetPos, cNodePos - targetPos, Color.red, 5.0f);

				if (Physics.Raycast(targetPos, cNodePos - targetPos, out hit, Vector3.Distance(targetPos,cNodePos), coverLayer.value)) 
                {
					//if(debugCover) Debug.Log ("Target: "+i+" Hit: "+hit.collider.gameObject.name);
                    //return (hit.collider.gameObject.name == "Cover");
                    good = (hit.collider.gameObject.GetComponent<Cover>() != null) && good;
                    //return (hit.transform == this.transform);
                }
                else 
					good = false;
			}
		}
        */
		return good;
	}
	
	public bool GoodCover(CoverNode cn, Vector3 movePos)
    {
		Vector3 cNodePos = cn.transform.position;
		RaycastHit hit;

		if(debugCover) Debug.DrawRay(movePos + Vector3.up, cNodePos - movePos, Color.blue, 5.0f);

		if (Physics.Raycast(movePos + Vector3.up, cNodePos - movePos, out hit, Vector3.Distance(movePos,cNodePos), coverLayer.value))
        {
			//return (hit.collider.gameObject.name == "Cover");
			return false;
			//return (hit.transform == this.transform);
		}
        else
			return true;
	}
}
