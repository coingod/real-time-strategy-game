using UnityEngine;
using System.Collections;

public class CoverNode : MonoBehaviour
{
	public bool inUse = false; //{ get; set; }

	public CoverSystem GetCover()
    {
		return transform.parent.parent.GetComponent<CoverSystem>();
	}
}

