﻿using UnityEngine;

public class CharacterAnimator : MonoBehaviour
{
	private Unit unit;
	private Animator anim;						// Reference to the Animator.
	private AnimatorHashIDs hash;					// Reference to the HashIDs script.

	
	// Use this for initialization
	private void Awake ()
	{
		unit = GetComponent<Unit>();
		anim = GetComponent<Animator>();
		hash = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<AnimatorHashIDs>();
	}
	
	// Update is called once per frame
	private void Update ()
	{
		//anim.SetFloat(hash.speedFloat, player.RunSpeed, 0.1f, Time.deltaTime);
		anim.SetBool(hash.movingBool, unit.IsMoving());
		anim.SetBool(hash.coverBool, unit.TakingCover());
		//anim.SetBool(hash.fireBool, unit.exposed);
	}
}
