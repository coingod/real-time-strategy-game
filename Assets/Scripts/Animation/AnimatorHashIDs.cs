﻿using UnityEngine;
using System.Collections;

public class AnimatorHashIDs : MonoBehaviour
{
	// Here we store the hash tags for various strings used in our animators.
	public int dyingState;
	public int locomotionState;
	public int shoutState;
	public int deadBool;
	public int speedFloat;
	public int sneakingBool;
	public int shoutingBool;
	public int playerInSightBool;
	public int shotFloat;
	public int aimWeightFloat;
	public int angularSpeedFloat;
	public int openBool;

	public int fireBool;
	public int coverBool;
	public int movingBool;
	
	
	void Awake ()
	{
		//dyingState = Animator.StringToHash("Base Layer.Dying");
		//locomotionState = Animator.StringToHash("Base Layer.Locomotion");
		//shoutState = Animator.StringToHash("Shouting.Shout");
		//deadBool = Animator.StringToHash("Dead");
		speedFloat = Animator.StringToHash("Speed");
		coverBool = Animator.StringToHash("Cover");
		fireBool = Animator.StringToHash("Fire");
		movingBool = Animator.StringToHash("Moving");
		//sneakingBool = Animator.StringToHash("Sneaking");
		//shoutingBool = Animator.StringToHash("Shouting");
		//playerInSightBool = Animator.StringToHash("PlayerInSight");
		//shotFloat = Animator.StringToHash("Shot");
		//aimWeightFloat = Animator.StringToHash("AimWeight");
		//angularSpeedFloat = Animator.StringToHash("AngularSpeed");
		//openBool = Animator.StringToHash("Open");
	}
}
