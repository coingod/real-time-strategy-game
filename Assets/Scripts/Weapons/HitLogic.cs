﻿using UnityEngine;
using System.Collections;

public class HitLogic : MonoBehaviour
{
    public Unit owner;
    private ParticleSystem part;
    private ParticleCollisionEvent[] collisionEvents;
    
    private void Start()
    {
        part = GetComponent<ParticleSystem>();
        collisionEvents = new ParticleCollisionEvent[16];
    }

    private void OnParticleCollision(GameObject other)
    {
        int safeLength = part.GetSafeCollisionEventSize();
        if (collisionEvents.Length < safeLength)
            collisionEvents = new ParticleCollisionEvent[safeLength];

        int numCollisionEvents = part.GetCollisionEvents(other, collisionEvents);
        Rigidbody rb = other.GetComponent<Rigidbody>();
        int i = 0;
        while (i < numCollisionEvents)
        {
            Vector3 pos = collisionEvents[i].intersection;

            if (rb)
            {
                
                Vector3 force = collisionEvents[i].velocity;
                rb.AddForce(force);
            }

            Quaternion rot = Quaternion.LookRotation(transform.position - pos);

            Unit target = null;
            //Did we hit a Unit?
            Transform hit_tr = other.transform;
            if (hit_tr)
            {
                target = hit_tr.GetComponent<Unit>();
                if (target)
                {
                    Instantiate(GameManager.Instance.bulletHitBloodFX, pos, rot);
                    target.Damage(owner, 25);
                }
                else
                {
                    Instantiate(GameManager.Instance.bulletHitSmokeFX, pos, rot);
                }
            }

            i++;
        }
    }
}
