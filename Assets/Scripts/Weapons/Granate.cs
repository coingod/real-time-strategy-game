﻿using UnityEngine;
using System.Collections;

public class Granate : MonoBehaviour
{
    public float explosionRadius = 10f;
    public float explosionForce = 1000f;
    public float fuseTime = 3.0f;
    public float maxDamage = 200;
    private float elapsedTime = 0;
    public GameObject explosionParticles;
    public AudioClip[] explosionSounds;
    public Unit owner;

    // Update is called once per frame
    private void Update()
    {
        elapsedTime += Time.deltaTime;

        if (elapsedTime > fuseTime)
        {
            if (explosionParticles)
            {
                GameObject gameObject2 = (GameObject)Instantiate(explosionParticles, transform.position, Quaternion.Euler(Vector3.up));

                gameObject2.GetComponent<AudioSource>().clip = explosionSounds[Random.Range(0, explosionSounds.Length - 1)];
                gameObject2.GetComponent<AudioSource>().Play();

                Instantiate(GameManager.Instance.explosionDecal, transform.position + Vector3.up * 0.02f, Quaternion.Euler(Vector3.left * 90));

                // Collect all the colliders in a sphere from the grenade's current position to a radius of the explosion radius.
                Collider[] colliders = Physics.OverlapSphere(transform.position, explosionRadius);

                // Go through all the colliders...
                for (int i = 0; i < colliders.Length; i++)
                {
                    // ... and find their rigidbody.
                    Rigidbody targetRigidbody = colliders[i].GetComponent<Rigidbody>();
                    // ... and the Unit component
                    Unit unit = colliders[i].GetComponent<Unit>();

                    // If it is indeed an Unit, damage it
                    if (unit)
                    {
                        // Calculate the amount of damage the target should take based on it's distance from the grenade.
                        float damage = CalculateDamage(unit.transform.position);

                        // Deal this damage to the Unit.
                        unit.Damage(owner, damage);

                        // Suppres this unit on twice that damage amount
                        unit.GetComponentInChildren<Suppresion>().Suppress(damage * 2);
                    }

                    // If they don't have a rigidbody, go on to the next collider.
                    if (!targetRigidbody)
                        continue;

                    // Add an explosion force.
                    targetRigidbody.AddExplosionForce(explosionForce, transform.position, explosionRadius);
                }
                
                // Destroy the grenade.
                Destroy(gameObject);

                /*
                Collider[] colliders = Physics.OverlapSphere(transform.position, radius);
                foreach (Collider hit in colliders)
                {
                    Rigidbody rb = hit.GetComponent<Rigidbody>();
                    Unit u = hit.GetComponent<Unit>();

                    if (u != null)
                        u.Damage(1000 / Vector3.Distance(transform.position, hit.transform.position));

                    if (rb != null)
                        rb.AddExplosionForce(power, transform.position, radius, 3.0F);
                }

                Destroy(gameObject);
                */
            }
        }
    }

    private float CalculateDamage(Vector3 targetPosition)
    {
        // Create a vector from the grenade to the target.
        Vector3 explosionToTarget = targetPosition - transform.position;

        // Calculate the distance from the grenade to the target.
        float explosionDistance = explosionToTarget.magnitude;

        // Calculate the proportion of the maximum distance (the explosionRadius) the target is away.
        float relativeDistance = (explosionRadius - explosionDistance) / explosionRadius;

        // Calculate damage as this proportion of the maximum possible damage.
        float damage = relativeDistance * maxDamage;

        // Make sure that the minimum damage is always 0.
        damage = Mathf.Max(0f, damage);

        return damage;
    }
}
