﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BulletSuppresion : MonoBehaviour
{
    private ParticleSystem m_System;
    private ParticleSystem.Particle[] m_Particles;
    private float radius;
    private Vector3 position;
    private Suppresion[] sList;
    public Unit owner;

    private void Start()
    {
        if (m_System == null)
            m_System = GetComponent<ParticleSystem>();

        if (m_Particles == null || m_Particles.Length < m_System.maxParticles)
            m_Particles = new ParticleSystem.Particle[m_System.maxParticles];

        sList = SuppresionColliders();
    }

    private void LateUpdate()
    {
        //return;
        int numParticlesAlive = m_System.GetParticles(m_Particles);

        for (int i = 0; i < numParticlesAlive; i++)
        {
            foreach(Suppresion s in sList)
            {
                if (!s)
                    continue;
                radius = s.GetComponent<SphereCollider>().radius;
                position = s.transform.position;
                //If the bullet is inside the Suppresion radius and the origin of the bullet is farter from it than 2 meters (to avoid being suppresed by own bullets)
                if (Vector3.Distance(m_Particles[i].position, position) < radius && Vector3.Distance(transform.position, position) > 2)
                {
                    s.GetComponent<Suppresion>().Suppress(owner);
                    //Debug.Log("Suppresed");
                }
            }
        }
    }

    private Suppresion[] SuppresionColliders()
    {
        //Find all Suppresion Colliders in Units
        Suppresion[] suppresions = FindObjectsOfType<Suppresion>();
        return suppresions;
    }
}