﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class Weapon : MonoBehaviour
{
    [Header("Weapon Properties")]
    public bool throwable = false;                          //Is this a throwable weapon?
    public int ammoPerClip;                                 //Amount of ammo in a new magazine
    public float reloadTime;                                //Time in seconds to reload
    public float rateOfFire = 2.0f;                         //Time in seconds between firing
    public float aimSpeed = 1.0f;                           //How quick this weapon can be aimed
    public float shotSpread = 0.1f;                         //Weapon accuracy (Max offset from target in meters)
    public Rigidbody grenade;                               //The Rigid Body that gets thrown

    [Header("Weapon Sounds")]
    public AudioClip[] fireSounds;                          //List of firing AudioClips
    public bool usesRingOutEffect = true;                   //If true play a diferent type of sound on last bullet
    public AudioClip[] ringOutSounds;                       //List of ringout AudioClips for last bullet
    public AudioClip reloadSound;                           //Reload AudioClip

    /** Private variables **/
    private Transform firePosition;
    private AudioSource audioSource;
    private ParticleSystem[] parts;
    private float currentWeaponChargeTime;
	private bool reloading = false;
    private int ammoLeftInClip;
    private Unit owner;
    private Suppresion ownerSuppresion;

    // Use this for initialization
    private void Start ()
	{
        parts = GetComponentsInChildren<ParticleSystem>();
        audioSource = GetComponent<AudioSource>();
        firePosition = transform.Find ("Muzzle Fire");
        //Simulate bullet spread by creating 2 curves (constants) for Max and Min spread
        //Letting the Particle System choose at random an XY velocity to get the effect.
        //https://forum.unity3d.com/threads/cant-set-any-new-particle-system-properties-through-script.358964/#post-2376054
        ParticleSystem bullet = firePosition.Find("Bullet").GetComponent<ParticleSystem>();
        ParticleSystem.VelocityOverLifetimeModule bulletVelocity = bullet.velocityOverLifetime;
        AnimationCurve curve = new AnimationCurve();
        curve.AddKey(0.0f, -shotSpread * 10);
        AnimationCurve curve2 = new AnimationCurve();
        curve2.AddKey(0.0f, shotSpread * 10);
        bulletVelocity.x = new ParticleSystem.MinMaxCurve(10.0f, curve, curve2);
        bulletVelocity.y = new ParticleSystem.MinMaxCurve(10.0f, curve, curve2);

        ammoLeftInClip = ammoPerClip;
		currentWeaponChargeTime = rateOfFire;

        bullet.GetComponent<HitLogic>().owner = owner;
        if(bullet.GetComponent<BulletSuppresion>())
            bullet.GetComponent<BulletSuppresion>().owner = owner;
    }
	
	// Update is called once per frame
	private void Update ()
	{
		currentWeaponChargeTime += Time.deltaTime;
	}
	
	public void SetOwner( Unit unit )
    {
		owner = unit;
        ownerSuppresion = unit.GetComponentInChildren<Suppresion>();
    }
	
    public void Fire()
    {
        //Check if weapon can be fired
        if (!ReadyToFire())
            return;

        currentWeaponChargeTime = 0.0f;
        ammoLeftInClip--;

        //If throwable, handle it different
        if (throwable)
        {
            Throw();
            return;
        }

        //Play sounds
        if (ammoLeftInClip == 1 && usesRingOutEffect)
            PlaySound(ringOutSounds);
        else
            PlaySound(fireSounds);

        //Emit particles
        foreach (ParticleSystem ps in parts)
        {
            ps.Emit(1);
        }
    }

    //This function should recieve a Vector3 target -> No need for accesing owner
    public void Throw()
    {
        Vector3 spawnPoint = firePosition.position;
        float time = Random.Range(1.5f, 2.0f);
        Vector3 direction = CalculateBestThrowSpeed(spawnPoint, owner.target.position + RandomError() * shotSpread, time);

        Rigidbody projectile = Instantiate(grenade, spawnPoint, Quaternion.identity) as Rigidbody;
        projectile.GetComponent<Granate>().owner = owner;
        projectile.AddForce(direction, ForceMode.VelocityChange);
    }

    private Vector3 RandomError()
    {
        return new Vector3(Random.Range(-1,1), Random.Range(-1, 1), 0);
    } 

    /*
    private float EffectiveRateOfFire()
    {
        float factor = Mathf.Clamp(ownerSuppresion.level, 10f, 100f);
        return rateOfFire * 100 / factor;
    }
    */

    public bool ReadyToFire()
    {
		if(HasAmmo())
        {
			if(currentWeaponChargeTime >= rateOfFire)//EffectiveRateOfFire())
                return true;
		}
        else if(!reloading)
            StartCoroutine("Reload");
		
		return false;
	}

	public bool HasAmmo()
    {
		return (ammoLeftInClip > 0);
	}

    public int AmmoLeft()
    {
        return ammoLeftInClip;
    }

    public bool Reloading()
    {
        return reloading;
    }

	public IEnumerator Reload()
    {
		reloading = true;
        //audioSource.clip = reloadSound;
        //audioSource.Play();

        //Notify owner that we are reloading
        owner.OnWeaponReload();

        yield return new WaitForSeconds (reloadTime);
		reloading = false;
		ammoLeftInClip = ammoPerClip;
		currentWeaponChargeTime = 0.0f;
	}

	protected void PlaySound(AudioClip[] clips)
    {
        audioSource.clip = clips[Random.Range(0,clips.Length-1)];
        audioSource.Play();
	}

    private Vector3 CalculateBestThrowSpeed(Vector3 origin, Vector3 target, float timeToTarget)
    {
        // calculate vectors
        Vector3 toTarget = target - origin;
        Vector3 toTargetXZ = toTarget;
        toTargetXZ.y = 0;

        // calculate xz and y
        float y = toTarget.y;
        float xz = toTargetXZ.magnitude;

        // calculate starting speeds for xz and y. Physics forumulase deltaX = v0 * t + 1/2 * a * t * t
        // where a is "-gravity" but only on the y plane, and a is 0 in xz plane.
        // so xz = v0xz * t => v0xz = xz / t
        // and y = v0y * t - 1/2 * gravity * t * t => v0y * t = y + 1/2 * gravity * t * t => v0y = y / t + 1/2 * gravity * t
        float t = timeToTarget;
        float v0y = y / t + 0.5f * Physics.gravity.magnitude * t;
        float v0xz = xz / t;

        // create result vector for calculated starting speeds
        Vector3 result = toTargetXZ.normalized;        // get direction of xz but with magnitude 1
        result *= v0xz;                                // set magnitude of xz to v0xz (starting speed in xz plane)
        result.y = v0y;                                // set y to v0y (starting speed of y plane)

        return result;
    }
}

