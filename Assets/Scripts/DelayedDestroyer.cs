﻿using UnityEngine;

public class DelayedDestroyer : MonoBehaviour
{
    public float timeToDestroy = 1.5f;

	private void Start () 
    {
        Destroy(gameObject, timeToDestroy);
	}
}
